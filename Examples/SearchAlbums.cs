using System;
using System.Threading.Tasks;
using YandexMusicApi.Api;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Examples
{
    public class SearchAlbums
    {
        private static readonly ApiParams ApiParams = new();

        public static async Task Main()
        {
            Search searchApi = new Search(ApiParams);

            Console.Write("Enter your search term: ");
            var searchRequest = Console.ReadLine();
            var searchResultJson = await searchApi.RetrieveSearch(searchRequest, typeSearch: SearchEnums.TypeSearch.Albums); // Search for your search query with album filter
            var searchResult = searchResultJson["result"]["result"];
            
            foreach (var i in searchResult["results"])
            {
                Console.WriteLine($"Title: {i["album"]["title"]}\nArtist: {i["album"]["artists"][0]["name"]}\nAlbum image link: {"https://" + i["album"]["coverUri"].ToString().Replace("%%", "100x100")}\n");
            }
        }
    }
}
using YandexMusicApi.Api;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.ComponentModels.Radio;
using YandexMusicApi.Network;

class Program
{
    public static async Task Main()
    {
        ApiParams apiParams = new ApiParams();
        apiParams.TokenYandexMusic = "YOUR_TOKEN";

        Radio radio = new Radio(apiParams);
        string radioId = "user:onyourwave";

        // var possibleSettingsRadio = await radio.GetWaveSettings(radioId); 
        // This query will help you get all possible radio settings, in this case My Wave
        var radioSession = await radio.NewSession(new List<string>()
        {
            radioId, // required
            // Now we can customize the radio as we want
            "settingLanguage:without-words",
            "settingDiversity:favorite",
            "settingMoodEnergy:active"
        });

        var radioSessionId = radioSession["result"]["result"]["radioSessionId"].ToString();
        var batchId = radioSession["result"]["result"]["batchId"].ToString();

        List<TracksList> tracksList = new List<TracksList>();
        foreach (var track in radioSession["result"]["result"]["sequence"])
        {
            tracksList.Add(new TracksList()
            {
                AlbumId = Convert.ToInt32(track["track"]["albums"][0]["id"]),
                Id = Convert.ToInt32(track["track"]["id"])
            });
        }

        // Assume you listened to all the tracks to the end, didn't skip any, and didn't like/dislike any. This example is provided so you know how to fill in the List<FeedbackData>
        /*
        var feedbackData = new List<FeedbackData>()
        {
            new RadioStartedFeedbackData(), // This is mandatory, but when you call the Feedback method the second time, it's not needed
            new TrackStartedFeedbackData()
            {
                BatchId = batchId,
                TrackId = tracksList[0].Id
            },
            new TrackFinishedFeedbackData()
            {
                BatchId = batchId,
                TotalPlayedSecond = 10f, // Here you need to specify how long you listened to the track
                TrackLengthSeconds = 20f, // And here how long the track lasts, I used this value as an example, getting the track length for this example seems excessive
                TrackId = tracksList[0].Id
            },
        }
        */

        var feedbackData = new List<FeedbackData> { new RadioStartedFeedbackData() };
        foreach (var i in tracksList)
        {
            feedbackData.Add(new TrackStartedFeedbackData()
            {
                BatchId = batchId,
                TrackId = i.Id
            });
            feedbackData.Add(new TrackFinishedFeedbackData()
            {
                BatchId = batchId,
                TotalPlayedSecond = 10f,
                TrackLengthSeconds = 20f,
                TrackId = i.Id
            });
        }

        var feedback = await radio.Feedback(radioSessionId, feedbackData); // If you want, you can pass 'from', which can be obtained from radioSession
        Console.WriteLine(feedback); // radio.Feedback will return a new batch of tracks
        
        // Then repeat the cycle: listen to tracks, like or dislike them, skip them, collect all this information in List<FeedbackData>, and send it through the radio.Feedback method
    }
}
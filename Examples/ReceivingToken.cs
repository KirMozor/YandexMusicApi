using Newtonsoft.Json.Linq;
using YandexMusicApi.Auth;
using YandexMusicApi.Network;

namespace ConsoleApp1;

class Program
{
    private static Token tokenObject = new Token(new ApiParams());

    public static async Task Main()
    {
        var loginResponse = await tokenObject.LoginUsername("YourUsername");
        Console.WriteLine(loginResponse.Data.ToString());

        var preferredAuthMethod = loginResponse.Data["preferred_auth_method"].ToString();
        switch (preferredAuthMethod)
        {
            case "password":
                await LoginPassword();
                break;
            // Add other cases for different auth methods as needed
            default:
                Console.WriteLine($"Unknown auth method: {preferredAuthMethod}");
                break;
        }

    }

    private static async Task LoginPassword()
    {
        var loginResponse = await tokenObject.LoginPassword("YourPassword");
        Console.WriteLine(loginResponse.Data.ToString());

        var status = loginResponse.Data["status"].ToString();
        switch (status)
        {
            case "ok":
                if (loginResponse.Data.ContainsKey("redirect_url"))
                    await CheckChallenge();
                await GetToken();
                break;
            case "error":
                var error = loginResponse.Data["errors"][0].ToString();
                await HandlerErrorLoginPassword(error);
                break;
            default:
                Console.WriteLine($"Unknown status: {status}");
                break;
        }
    }

    private static async Task GetToken()
    {
        var token = await tokenObject.GetToken();
        var tokenData = token.Data;

        switch (tokenData["status"].ToString())
        {
            case "ok":
                Console.WriteLine(tokenData);
                break;
            case "error":
                Console.WriteLine(tokenData);
                break;
            case "error_access_account":
                GrantPermission();
                break;
        }
    }

    private static async Task CheckChallenge()
    {
        var rawLoginResponse = await tokenObject.CheckChallenge();
        Console.WriteLine(rawLoginResponse.Data.ToString());
        var loginResponseJson = JObject.Parse(rawLoginResponse.Data.ToString());

        switch (loginResponseJson["challenge"]["challengeType"].ToString())
        {
            case "phone_confirmation":
                await PhoneConfirmation();
                break;
            case "mobile_id":
                await MobileId();
                break;
            case "email_code":
                await EmailCode();
                break;
            default:
                Console.WriteLine($"Unknown challenge type: {loginResponseJson["challenge"]["challengeType"]}");
                break;
        }
    }

    private static async Task PhoneConfirmation()
    {
        var phoneConfirm = await tokenObject.PhoneConfirmCodeSubmit();
        Console.WriteLine(phoneConfirm.Data.ToString());
        if (phoneConfirm.Data["status"].ToString() == "ok")
        {
            while (true)
            {
                Console.WriteLine("Enter the code from the SMS");
                var smsCode = Console.ReadLine();

                var phoneConfirmCode = await tokenObject.PhoneConfirmCodeSend(smsCode);
                if (phoneConfirmCode.Data["status"].ToString() == "ok")
                {
                    await tokenObject.CommitChallenge();
                    //await GetToken();
                    break;
                }
                else
                    Console.WriteLine("The SMS code is incorrect. Try again.");
            }
        }
        else
            Console.WriteLine("Phone number could not be verified. Please try again.");
    }

    private static async Task MobileId()
    {
        int attemptCount = 0;
        while (true)
        {
            if (attemptCount % 3 == 0) // Request a new code after every three unsuccessful attempts
            {
                var mobileIdResponse = await tokenObject.MobileIdGetCode();
                Console.WriteLine(mobileIdResponse.Data.ToString());
            }

            Console.WriteLine(
                "Enter the code from the PUSH notification, if it asks you to get the code via SMS, enter Y");
            var smsCode = Console.ReadLine();
            if (smsCode.ToLower() == "y")
            {
                await PhoneConfirmation();
                break;
            }
            else
            {
                if (!int.TryParse(smsCode, out int code))
                {
                    Console.WriteLine("An invalid code has been entered. Please try again.");
                    attemptCount++;
                    continue;
                }

                var mobileIdConfirm = await tokenObject.MobileIdConfirm(code);
                Console.WriteLine(mobileIdConfirm.Data.ToString());

                if (mobileIdConfirm.Data["status"].ToString() != "ok")
                {
                    Console.WriteLine("MobileId could not be confirmed. Please try again.");
                    attemptCount++;
                }
                else
                {
                    await tokenObject.CommitChallenge();
                    break;
                }
            }
        }
    }

    private static async Task EmailCode()
    {
        var startEmailChallenge = await tokenObject.SendEmailCode();
        if (startEmailChallenge.Data["result"]["status"].ToString() == "ok")
        {
            while (true)
            {
                Console.WriteLine("You should receive a code by email, enter it, if you want to get the code again, enter Y:");
                var code = Console.ReadLine();

                if (code.ToLower() == "y")
                {
                    await tokenObject.SendEmailCode();
                    continue;
                }

                var sendEmailCode = await tokenObject.CheckEmailCode(Convert.ToInt32(code));
                Console.WriteLine(sendEmailCode.Data);

                if (sendEmailCode.Data["result"]["status"].ToString() == "ok")
                {
                    await tokenObject.CommitChallenge();
                    break;
                }
                else
                {
                    Console.WriteLine("You've entered the wrong code, try again");
                    await tokenObject.SendEmailCode();
                }
            }
        }
        else
            Console.WriteLine("Failed to send a request to receive an email code, or something is wrong in your code, or something has changed in the authorization on the Yandex side");
    }

    private static async Task HandlerErrorLoginPassword(string error)
    {
        switch (error)
        {
            case "password.not_matched":
                Console.WriteLine(
                    "Your password is not appropriate for this account. Please change your password and try again.");
                break;
            case "captcha.required":
                await Captcha();
                break;
        }
    }

    private static async Task Captcha()
    {
        int attemptCount = 0;
        while (true)
        {
            if (attemptCount % 3 == 0) // Request a new captcha after every three unsuccessful attempts
            {
                var captchaResponse = await tokenObject.GetCaptcha();
                Console.WriteLine(captchaResponse.Data.ToString());
            }

            Console.WriteLine("Enter the answer to the captcha or enter Y to exit");
            var captchaAnswer = Console.ReadLine();

            if (captchaAnswer.ToLower() == "y")
                break;

            var sendCaptchaResponse = await tokenObject.SendCaptcha(captchaAnswer);
            var status = sendCaptchaResponse.Data["status"].ToString();
            if (status == "ok")
            {
                await Main();
                break;
            }
            else if (status == "error")
            {
                Console.WriteLine("Error when entering captcha. Try again.");
                attemptCount++;
            }
        }
    }

    private static void GrantPermission()
    {
        Console.WriteLine(
            "Alas, Yandex has requested permission for YandexMusic to access your account. I can't solve this problem yet (if there is a solution, please post it in the thread on GitLab).\nThe only solution at the moment is to log in using this link (https://oauth.yandex.ru/authorize?response_type=token&client_id=23cabbbdc6cd418abb4b39c32c41195d) and allow access, after which the token can be retrieved without problems");
    }
}

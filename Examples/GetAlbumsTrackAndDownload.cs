using TagLib;
using YandexMusicApi.Api;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;
using File = System.IO.File;

namespace ConsoleApp1
{
    class Program
    {
        private static readonly ApiParams ApiParams = new();
        private static readonly HttpClient Client = new(); // Singleton HttpClient

        public static async Task Main()
        {
            var albumApi = new Album(ApiParams);
            var trackApi = new Track(ApiParams);
            
            var bestTracksResult = await albumApi.GetTracks(5568718); // Get the top 5 tracks from the Evolve - Imagine Dragons album
            var bestTracksList = bestTracksResult["result"]["result"]["bests"].ToList();

            foreach (var trackId in bestTracksList.Select(i => Convert.ToInt32(i)))
            {
                var trackInfo = await trackApi.GetInformTrack(new List<int> { trackId }); // Getting track information
                var trackInfoJson = trackInfo["result"]["result"][0];

                // Obtaining information for subsequent writing to metadata
                var artistName = trackInfoJson["artists"][0]["name"].ToString();
                var trackName = trackInfoJson["title"].ToString();
                var albumImageUrl = $"https://{trackInfoJson["albums"][0]["coverUri"]}";
                albumImageUrl = albumImageUrl.Replace("%%", "300x300");
                var genre = trackInfoJson["albums"][0]["genre"].ToString();
                var albumName = trackInfoJson["albums"][0]["title"].ToString();
                var year = trackInfoJson["albums"][0]["year"].ToString();

                // Get a direct link to a track
                var directLink = await trackApi.FetchTrackLink(trackId, TrackQuality.High);
                var codec = directLink["result"]["result"]["downloadInfo"]["codec"].ToString();
                var link = directLink["result"]["result"]["downloadInfo"]["url"].ToString();

                Console.WriteLine($"{artistName} - {trackName}\nDirect link: {directLink}\nDownloading...");
                var fileName = await DownloadMusic(link, artistName, trackName, codec);

                // Set metadata for the downloaded file
                await SetMetadata(fileName, artistName, trackName, albumName, genre, year, albumImageUrl);
            }
        }

        private static async Task<string> DownloadMusic(string url, string artistName, string trackName, string extensions)
        {
            var randomWord = Guid.NewGuid().ToString().Substring(0, 5);

            switch (extensions)
            {
                case "he-aac":
                case "aac":
                    extensions = "m4a";
                    break;
            }
            
            var fileName = $"{artistName} - {trackName} [{randomWord}].{extensions}";

            try
            {
                using (var response = await Client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        using (var fileStream = await response.Content.ReadAsStreamAsync())
                        using (var outputFileStream = File.Create(fileName))
                        {
                            await fileStream.CopyToAsync(outputFileStream);
                        }
                        Console.WriteLine($"Download complete! Path: {Path.GetFullPath(fileName)}");
                        return fileName;
                    }
                    else
                    {
                        Console.WriteLine($"Error: {response.StatusCode}");
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine($"HTTP Request Error: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex}");
            }

            return null;
        }

        private static async Task SetMetadata(string fileName, string artistName, string trackName, string albumName, string genre, string year, string albumImageUrl)
        {
            if (fileName == null) return;

            var file = TagLib.File.Create(fileName);

            file.Tag.Performers = new[] { artistName };
            file.Tag.Title = trackName;
            file.Tag.Album = albumName;
            file.Tag.Genres = new[] { genre };
            file.Tag.Year = uint.TryParse(year, out var parsedYear) ? parsedYear : 0;

            // Set album art using HttpClient
            try
            {
                using (var response = await Client.GetAsync(albumImageUrl))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var albumArtData = await response.Content.ReadAsByteArrayAsync();
                        file.Tag.Pictures = new[]
                        {
                            new Picture(new ByteVector(albumArtData))
                            {
                                Type = PictureType.FrontCover,
                                Description = "Album cover",
                                MimeType = response.Content.Headers.ContentType.ToString()
                            }
                        };
                    }
                    else
                    {
                        Console.WriteLine($"Failed to download album art: {response.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error downloading album art: {ex.Message}");
            }

            file.Save();
            Console.WriteLine($"Metadata saved to file: {fileName}");
        }
    }
}
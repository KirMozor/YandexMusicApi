<h1>YandexMusicApi</h1>

<img src="https://download.cdn.yandex.net/from/yandex.ru/support/ru/music/files/icon_main.png" width="7%">
<h3>Cross-platform Yandex Music API for C#</h3>
<h2>  
   <a href="https://www.nuget.org/packages/YandexMusicApi/">  
      <img src="https://img.shields.io/nuget/dt/YandexMusicApi?style=flat">  
   </a>
   <a href="https://gitlab.com/KirMozor/YandexMusicApi/-/blob/main/YandexMusicApi.csproj">  
      <img src="https://img.shields.io/badge/.NET-Standard%202.0-blueviolet?style=flat">  
   </a>
   <img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103?style=flat" >
   <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
   <a href="https://mastodon.social/@kirmozor">  
      <img src="https://img.shields.io/mastodon/follow/108763242400809137">  
   </a>
   <img src="https://img.shields.io/gitlab/stars/Xeronix/YandexMusicApi.svg?style=flat">
</h2>

## Description⚡

The YandexMusicApi library in C# allows for easy and convenient use of the Yandex.Music API to develop various music-related projects. It supports .NET Standard 2.0 and is available in a [NuGet package that has been downloaded over 4,000 times.](https://www.nuget.org/packages/YandexMusicApi/)


With this library, developers can access a wide range of Yandex.Music API functions, such as searching for songs, albums, artists, playlists, obtaining track information, obtaining links for streaming playback, and more. All API functions are provided in a user-friendly and easy-to-use form.

In addition, [this library is a fork of the original library written in Python by Marshal](https://github.com/MarshalX/yandex-music-api), which allows it to be used on C# for developing music projects.

The development of the library was based on the [website of Alexandr Cherkashin](https://github.com/acherkashin/yandex-music-open-api), where the Yandex.Music API is presented in the form of OpenAPI.


Overall, the YandexMusicApi library in C# is a powerful tool for developing applications and projects related to music on the Yandex.Music platform.


## How to install✨

Installation instructions for YandexMusicApi:

1. Install the .NET Core SDK on your computer if it is not already installed. You can download it from the official website: https://dotnet.microsoft.com/download

2. Open the terminal or command prompt on your computer.

3. Enter the following command in the terminal or command prompt:

    * dotnet add package YandexMusicApi

4. Wait for the installation to complete. After that, you can use the YandexMusicApi library in your C# project.

That's it! Now you can use the YandexMusicApi library to access the functions of the Yandex.Music API in your C# project.

## Example of use🍏

All examples on API usage will be located [in the Examples project folder](https://gitlab.com/Xeronix/YandexMusicApi/-/tree/main/Examples)

## Documentation

There is no online documentation for this project yet. But the documentation is in the code itself, you can look there😉

## How to run tests

To run the tests, you need to go to the YandexMusicApi.Tests directory and add your token using the following command:

```bash
dotnet user-secrets set API_TOKEN_YANDEX_MUSIC "YOUR_TOKEN"
```

And then run Unit tests through the nunit-console or through your code editor

## Community

My channel on Telegram: https://t.me/itOpenSource

There is also a mirror in the Mastodon: https://mastodon.social/@kirmozor

We also have a chat room, the link is in the attached message of the channel😁

## Sponsors
This project is sponsored by JetBrains and GitLab. Thanks to them, I have a license for the IDE and all the features of GitLab Ultimate🙂

   [![JetBrains icon](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg)](https://www.jetbrains.com/)
   [![JetBrains Rider icon](https://resources.jetbrains.com/storage/products/company/brand/logos/Rider.svg)](https://www.jetbrains.com/rider/)
   [![GitLab icon](https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg)](https://about.gitlab.com/why-gitlab/)

## Helping the channel and development

If you want to help this project as well as my channels, the details are below:
* Ton - EQDD8dqOzaj4zUK6ziJOo_G2lx6qf1TEktTRkFJ7T1c_fPQb
  * Tag/Memo: 11566437
* Btc - 1JD1cmk9d7F5rxJqZZqc3AtGrpYmPZCP7s
* Tether (TRC20) - TGNNEcWHEd2caBSShBBeWheGoyZwDtjAgT
* Tether (BEP20) - 0x5b45af96923e94ab79127ade390fa1c869b02a65
* BNB (BEP20) - 0x5b45af96923e94ab79127ade390fa1c869b02a65
* Visa - 4400430240807740

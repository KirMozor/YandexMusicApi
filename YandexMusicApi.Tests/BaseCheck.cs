using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace YandexMusicApi.Tests;

public class BaseCheck
{
    protected readonly IConfigurationRoot ConfigurationRoot;
    protected BaseCheck()
    {
        ConfigurationRoot = new ConfigurationBuilder()
            .AddUserSecrets<BaseCheck>()
            .Build();
    }
    protected bool CheckIsFailed(JObject resultResponse)
    {
        Console.WriteLine(resultResponse);

        if (resultResponse["status"]?.ToString() != "OK")
            return true;
        if (!resultResponse.TryGetValue("result", out var result))
            return true;
        if (result["result"] == null)
            return false;

        var innerResult = result["result"];
        
        // Check that innerResult is non-null
        if (innerResult != null)
        {
            // Check that the type is not Null
            if (innerResult.Type == JTokenType.Null)
                return true;

            // Check that this is a string and it is empty or null
            if (innerResult.Type == JTokenType.String)
            {
                string innerResultString = innerResult.ToString();
                if (string.IsNullOrWhiteSpace(innerResultString) || string.IsNullOrEmpty(innerResultString))
                    return true;
            }
        }

        return false; // Successful checking
    }
}
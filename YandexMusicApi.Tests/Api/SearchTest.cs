using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

public class SearchTest : BaseCheck
{
    private ApiParams _apiParams;
    private string _userId;
    private YandexMusicApi.Api.Search _search;
    
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _search = new YandexMusicApi.Api.Search(_apiParams);
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test, Order(1)]
    public async Task GetSearchHistory()
    {
        var result = await _search.GetSearchHistory(_userId, SearchEnums.SearchType.All);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(2)]
    public async Task ClearSearchHistory()
    {
        var result = await _search.ClearSearchHistory(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task Search()
    {
        var searchQuery = "Skrillex";
        var noCorrect = true;
        var page = 0;
        var pageSize = 20;
        var typeSearch = SearchEnums.TypeSearch.Artists;

        var result = await _search.RetrieveSearch(searchQuery, noCorrect, page, pageSize, typeSearch);

        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
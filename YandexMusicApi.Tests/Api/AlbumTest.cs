using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class AlbumTest : BaseCheck
{
    private ApiParams _apiParams;
    private YandexMusicApi.Api.Album _album;
    private string _userId;
    
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _album = new YandexMusicApi.Api.Album(_apiParams);
        
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test]
    public async Task InformAlbum()
    {
        var result = await _album.InformAlbum(123);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetTracks()
    {
        var result = await _album.GetTracks(123, 0, 5);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task InformAlbums()
    {
        var result = await _album.InformAlbums(new List<int>()
        { 123, 543, 654 });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task LikesAlbums()
    {
        var result = await _album.LikesAlbums(new List<int>()
        { 123, 543, 654 }, _userId);
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetLikesAlbum()
    {
        var result = await _album.GetLikesAlbum(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task RemoveLikesAlbums()
    {
        var result = await _album.RemoveLikesAlbums(new List<int>()
        {
            123, 543, 654
        }, _userId);
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
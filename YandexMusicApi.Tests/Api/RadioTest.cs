using YandexMusicApi.Api;
using YandexMusicApi.ComponentModels.Radio;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class RadioTest : BaseCheck
{
    private ApiParams _apiParams;
    private Radio _radio;
    private Track _track;

    private const string IdWave = "user:onyourwave";
    private string? _radioSessionId;
    private string? _batchId;
    private List<int> _trackId = new();
    
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];
        _radio = new Radio(_apiParams);
        _track = new Track(_apiParams);
    }

    [Test]
    public async Task StationList()
    {
        var result = await _radio.StationList();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task WaveLast()
    {
        var result = await _radio.WaveLast();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetSettingsWave()
    {
        var result = await _radio.GetWaveSettings(IdWave);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(1)]
    public async Task NewSession()
    {
        var settingsWave = new List<string>()
        {
            "mood:summer",
            "settingMoodEnergy:active",
            "settingDiversity:favorite",
            "settingLanguage:not-russian"
        };
        
        var result = await _radio.NewSession(settingsWave);
        if (CheckIsFailed(result))
            Assert.Fail();
        
        _radioSessionId = result["result"]["result"]["radioSessionId"].ToString();
        _batchId = result["result"]["result"]["batchId"].ToString();
        foreach (var i in result["result"]["result"]["sequence"])
        {
            _trackId.Add(Convert.ToInt32(i["track"]["id"]));
        }
    }
    
    [Test, Order(2)]
    public async Task GetInformAboutWave()
    {
        var result = await _radio.GetInformAboutRadio(_radioSessionId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(3)]
    public async Task RadioStarted()
    {
        var feedbackData = new RadioStartedFeedbackData();
        var result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { feedbackData });
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(4)]
    public async Task TrackStarted()
    {
        var feedbackData = new TrackStartedFeedbackData
        {
            BatchId = _batchId,
            TrackId = _trackId[0]
        };
        var result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { feedbackData });
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(5)]
    public async Task TrackFinishedAndStartedNew()
    {
        var checkTrackLength = await _track.GetInformTrack(new List<int> { _trackId[0] });
        float trackLength = Convert.ToInt32(checkTrackLength["result"]["result"][0]["durationMs"]) / 1000f;

        var finishedFeedbackData = new TrackFinishedFeedbackData
        {
            BatchId = _batchId,
            TrackId = _trackId[0],
            TrackLengthSeconds = trackLength,
            TotalPlayedSecond = trackLength
        };

        var startedFeedbackData = new TrackStartedFeedbackData
        {
            BatchId = _batchId,
            TrackId = _trackId[1]
        };

        var result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { finishedFeedbackData });
        if (CheckIsFailed(result))
            Assert.Fail("Track completion data could not be sent");

        result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { startedFeedbackData });
        if (CheckIsFailed(result))
            Assert.Fail("Failed to send data to start a new track");
    }

    [Test, Order(6)]
    public async Task LikeTrack()
    {
        var feedbackData = new LikeFeedbackData
        {
            BatchId = _batchId,
            TrackId = _trackId[1]
        };
        var result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { feedbackData });
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(7)]
    public async Task UnlikeTrack()
    {
        var feedbackData = new UnlikeFeedbackData
        {
            BatchId = _batchId,
            TrackId = _trackId[1]
        };
        var result = await _radio.Feedback(_radioSessionId, new List<FeedbackData> { feedbackData });
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task MultipleFeedback()
    {
        var newSession = await _radio.NewSession(new List<string>()
        {
            "mood:summer",
            "settingMoodEnergy:active",
            "settingDiversity:favorite",
            "settingLanguage:not-russian"
        });
        if (CheckIsFailed(newSession))
            Assert.Fail("Unable to create a new session for radio");

        var radioSessionId = newSession["result"]["result"]["radioSessionId"].ToString();
        var batchId = newSession["result"]["result"]["batchId"].ToString();
        var trackId = new List<int>();
        foreach (var i in newSession["result"]["result"]["sequence"])
        {
            trackId.Add(Convert.ToInt32(i["track"]["id"]));
        }

        var feedbackData = new List<FeedbackData>()
        {
            new RadioStartedFeedbackData(),
            new TrackStartedFeedbackData()
            {
                BatchId = batchId,
                TrackId = trackId[0]
            }
        };

        var result = await _radio.Feedback(radioSessionId, feedbackData);
        if (CheckIsFailed(result))
            Assert.Fail("Could not send radio start and track start data");
    }
}
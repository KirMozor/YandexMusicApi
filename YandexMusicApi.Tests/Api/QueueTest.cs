using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.ComponentModels.Queue;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class QueueTest : BaseCheck
{
    private ApiParams _apiParams;
    
    private YandexMusicApi.Api.Queue _queue;
    private YandexMusicApi.Api.Playlist _playlist;
    private YandexMusicApi.Api.Album _album;
    private YandexMusicApi.Api.Artist _artist;
    private YandexMusicApi.Api.Radio _radio;
    private YandexMusicApi.Api.Track _track;
    
    private string _userId;
    private string? _queueId;
   
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _queue = new YandexMusicApi.Api.Queue(_apiParams);
        _playlist = new YandexMusicApi.Api.Playlist(_apiParams);
        _album = new YandexMusicApi.Api.Album(_apiParams);
        _artist = new YandexMusicApi.Api.Artist(_apiParams);
        _radio = new YandexMusicApi.Api.Radio(_apiParams);
        _track = new YandexMusicApi.Api.Track(_apiParams);
        
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test, Order(1)]
    public async Task GetQueue()
    {
        var result = await _queue.GetQueue();
        if (CheckIsFailed(result))
            Assert.Fail();
        _queueId = result["result"]["result"]["queues"][0]["id"].ToString();
    }

    [Test, Order(2)]
    public async Task GetQueueById()
    {
        var result = await _queue.GetQueueById(_queueId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(3)]
    public async Task UpdateQueue()
    {
        var result = await _queue.UpdateQueue(_queueId, 0);
        if (CheckIsFailed(result) || result["result"]["result"]["status"].ToString() != "ok")
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForTrack()
    {
        int idTrack = 42197229;
        var informTrack = await _track.GetInformTrack(new List<int> { idTrack });

        var trackList = new List<TracksList> { new()
            {
                Id = idTrack,
                AlbumId = Convert.ToInt32(informTrack["result"]["result"][0]["albums"][0]["id"])
            } 
        };
        
        string description = informTrack["result"]["result"][0]["title"].ToString();

        var result = await _queue.CreateQueue(new TrackQueue
        {
            Description = description,
            TracksList = trackList,
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForAlbum()
    {
        int id = 6017186;

        var getTracks = await _album.GetTracks(id);
        if (CheckIsFailed(getTracks))
            Assert.Fail();

        JArray listTracksJson = getTracks["result"]["result"]["volumes"][0] as JArray;
        List<TracksList> tracksList = new List<TracksList>();

        foreach (var track in listTracksJson)
        {
            var trackList = new TracksList()
            {
                Id = Convert.ToInt32(track["id"]),
                AlbumId = id
            };
            
            tracksList.Add(trackList);
        }

        string description = getTracks["result"]["result"]["title"].ToString();
        var result = await _queue.CreateQueue(new AlbumQueue()
        {
            Description = description,
            TracksList = tracksList,
            Id = id
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForArtist()
    {
        int id = 675068;

        var getTracks = await _artist.GetTracks(id);
        if (CheckIsFailed(getTracks))
            Assert.Fail();

        JArray listTracksJson = getTracks["result"]["result"]["tracks"] as JArray;
        List<TracksList> tracksList = new List<TracksList>();

        foreach (var track in listTracksJson)
        {
            var trackList = new TracksList() { Id = Convert.ToInt32(track["id"]) };
            var albumId = track["albums"] as JArray;

            if (albumId.Count != 0)
                trackList.AlbumId = Convert.ToInt32(albumId[0]["id"]);
            
            tracksList.Add(trackList);
        }

        string description = getTracks["result"]["result"]["tracks"][0]["artists"][0]["name"].ToString();

        var result = await _queue.CreateQueue(new ArtistQueue()
        {
            Description = description,
            TracksList = tracksList,
            Id = id
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForPlaylist()
    {
        int id = 103372440;
        int kind = 2710;
        
        var getTracks = await _playlist.InformPlaylistAndTracks(id.ToString(), kind);
        if (CheckIsFailed(getTracks))
            Assert.Fail();

        JArray listTracksJson = getTracks["result"]["result"]["tracks"] as JArray;
        List<TracksList> tracksList = new List<TracksList>();

        foreach (var track in listTracksJson)
        {
            var trackList = new TracksList { Id = Convert.ToInt32(track["id"]) };
            var albumId = track["track"]["albums"] as JArray;

            if (albumId.Count != 0)
                trackList.AlbumId = Convert.ToInt32(albumId[0]["id"]);
            
            tracksList.Add(trackList);
        }

        string description = getTracks["result"]["result"]["title"].ToString();

        var result = await _queue.CreateQueue(new PlaylistQueue()
        {
            Description = description,
            TracksList = tracksList,
            PlaylistId = kind,
            UserId = id
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForMyMusic()
    {
        int kind = 3;
        
        var getTracks = await _playlist.InformPlaylistAndTracks(_userId, kind);
        if (CheckIsFailed(getTracks))
            Assert.Fail();

        JArray listTracksJson = getTracks["result"]["result"]["tracks"] as JArray;
        List<TracksList> tracksList = new List<TracksList>();

        foreach (var track in listTracksJson)
        {
            var trackList = new TracksList() { Id = Convert.ToInt32(track["id"]) };
            var albumId = track["track"]["albums"] as JArray;

            if (albumId.Count != 0)
                trackList.AlbumId = Convert.ToInt32(albumId[0]["id"]);
            
            tracksList.Add(trackList);
        }

        string description = getTracks["result"]["result"]["title"].ToString();

        var result = await _queue.CreateQueue(new MyMusicQueue()
        {
            Description = description,
            TracksList = tracksList,
            UserId = _userId
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task CreateQueueForRadio()
    {
        string id = "user:onyourwave";
        
        var createRadioSession = await _radio.NewSession(new List<string> { id });

        var radioSessionId = createRadioSession["result"]["result"]["radioSessionId"].ToString();
        var description = createRadioSession["result"]["result"]["wave"]["name"].ToString();

        var result = await _queue.CreateQueue(new RadioQueue()
        {
            Description = description,
            Id = id,
            RadioSessionId = radioSessionId
        });
        
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
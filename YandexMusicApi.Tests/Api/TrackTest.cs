using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

public class TrackTest : BaseCheck
{
    private ApiParams _apiParams;
    private string _userId;
    private YandexMusicApi.Api.Track _track;
    
    private readonly List<int> _tracksList = new()
    {
        178532, 50569328
    };
    private string? _downloadInfoUrl;
    
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];
        
        _track = new YandexMusicApi.Api.Track(_apiParams);
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test, Order(1)]
    public async Task AddLikesTrack()
    {
        var result = await _track.AddLikesTracks(_tracksList, _userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(2)]
    public async Task RemoveLikesTrack()
    {
        var result = await _track.RemoveLikesTracks(_tracksList, _userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetLikesTrack()
    {
        var result = await _track.GetLikesTrack(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetInformTrack()
    {
        var result = await _track.GetInformTrack(_tracksList);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(1)]
    public async Task GetDownloadInfo()
    {
        var result = await _track.GetDownloadInfo(_tracksList[0]);
        if (CheckIsFailed(result))
            Assert.Fail();

        _downloadInfoUrl = result["result"]["result"][0]["downloadInfoUrl"].ToString();
    }

    [Test, Order(2)]
    public async Task GetDirectLink()
    {
        try
        {
            var result = await _track.GetDirectLink(_downloadInfoUrl);
            Console.WriteLine(result);
        }
        catch (System.Xml.XmlException ex)
        {
            Assert.Fail($"XML parsing error: {ex}");
        }
    }

    [Test]
    public async Task GetTrackSimilar()
    {
        var result = await _track.GetTrackSimilar(_tracksList[0]);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetDislikesTracks()
    {
        var result = await _track.GetDislikesTracks(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetSupplement()
    {
        var result = await _track.GetSupplement(_tracksList[0]);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task FetchTrackLink()
    {
        var result = await _track.FetchTrackLink(_tracksList[0], TrackQuality.Low);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetLyric()
    {
        var result = await _track.GetLyric(_tracksList[0]);
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
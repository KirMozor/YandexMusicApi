using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class ArtistTest : BaseCheck
{
    private ApiParams _apiParams;
    private YandexMusicApi.Api.Artist _artist;
    private string _userId;
    
    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _artist = new YandexMusicApi.Api.Artist(_apiParams);
        
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test]
    public async Task GetTracks()
    {
        var result = await _artist.GetTracks(428038, 0, 5, true);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetBriefInfo()
    {
        var result = await _artist.GetBriefInfo(428038);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetAlbums()
    {
        var result = await _artist.GetAlbums(428038, 0, 5, sortBy: ArtistEnums.SortBy.Rating);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task InformArtist()
    {
        var result = await _artist.InformArtist(428038);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task LikesArtists()
    {
        var result = await _artist.LikesArtists(new List<int>() 
            { 428038 }, _userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetLikesArtists()
    {
        var result = await _artist.GetLikesArtist(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task RemoveLikesArtists()
    {
        var result = await _artist.RemoveLikesArtists(new List<int>()
        { 428038 }, _userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task FamiliarYou()
    {
        var result = await _artist.FamiliarYou(428038);
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
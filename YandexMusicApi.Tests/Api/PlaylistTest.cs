using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class PlaylistTest : BaseCheck
{
    private ApiParams _apiParams;
    private YandexMusicApi.Api.Playlist _playlist;
    private string _userId;
    private int _playlistId = 1000;
    private int _revision;
    
    // Imagine Dragons - Believer
    private readonly List<TracksList> _tracksLists = [new TracksList { Id = 33311009, AlbumId = 5568718 }];

    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _playlist = new YandexMusicApi.Api.Playlist(_apiParams);
        
        var account = new YandexMusicApi.Api.Account(_apiParams);
        var resultGetId = await account.ShowInformAccount();
        _userId = resultGetId["result"]["result"]["account"]["uid"].ToString();
    }

    [Test]
    public async Task GetLikesPlaylists()
    {
        var result = await _playlist.GetLikesPlaylists(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task ListUserPlaylists()
    {
        var result = await _playlist.ListUserPlaylists(_userId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(1)]
    public async Task CreatePlaylist()
    {
        var result = await _playlist.CreatePlaylist(_userId, "Checking unit test", true);
        if (CheckIsFailed(result))
            Assert.Fail();
        else
            _playlistId = Convert.ToInt32(result["result"]["result"]["kind"]);
    }

    [Test, Order(2)]
    public async Task ChangeNamePlaylist()
    {
        var result =
            await _playlist.ChangeNamePlaylist(_userId, _playlistId, 
                "New name playlist for checking unit test");
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(3)]
    public async Task InformPlaylistAndTracks()
    {
        var result = await _playlist.InformPlaylistAndTracks(_userId, _playlistId, 0, 5);
        if (CheckIsFailed(result))
            Assert.Fail();
        else
            _revision = Convert.ToInt32(result["result"]["result"]["revision"]);
    }

    [Test, Order(4)]
    public async Task AddTracks()
    {
        var result = await _playlist.AddTracks(_userId, _playlistId, _tracksLists, _revision);
        if (CheckIsFailed(result))
            Assert.Fail();
        else
            _revision++;
    }

    [Test, Order(5)]
    public async Task RemoveTracks()
    {
        var result = await _playlist.RemoveTracks(_userId, _playlistId, _tracksLists, _revision);
        if (CheckIsFailed(result))
            Assert.Fail();
        else
            _revision++;
    }

    [Test, Order(6)]
    public async Task GetRecommendations()
    {
        var result = await _playlist.GetRecommendations(_userId, _playlistId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test, Order(7)]
    public async Task ChangeVisibility()
    {
        var result = await _playlist.ChangeVisibility(_userId, _playlistId, true);
        if (CheckIsFailed(result))
            Assert.Fail();
    }
    
    [Test, Order(8)]
    public async Task DeletePlaylist()
    {
        var result = await _playlist.DeletePlaylist(_userId, _playlistId);
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class LandingTest : BaseCheck
{
    private ApiParams _apiParams;
    private YandexMusicApi.Api.Landing _landing;
    
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _landing = new YandexMusicApi.Api.Landing(_apiParams);
    }

    [Test]
    public async Task GetSkeleton()
    {
        var result = await _landing.GetSkeleton();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task GetBlock()
    {
        foreach (LandingEnums.BlockTypes blockType
                 in Enum.GetValues(typeof(LandingEnums.BlockTypes)))
        {
            var result = await _landing.GetBlock(blockType);
            if (CheckIsFailed(result))
                Assert.Fail();
        }
    }

    [Test]
    public async Task GetEditorialPromotionBlock()
    {
        foreach (LandingEnums.EditorialPromotionBlockTypes blockType
                 in Enum.GetValues(typeof(LandingEnums.EditorialPromotionBlockTypes)))
        {
            var result = await _landing.GetEditorialPromotionBlock(blockType);
            if (CheckIsFailed(result))
                Assert.Fail();
        }
    }

    [Test]
    public async Task StationDashboard()
    {
        var result = await _landing.StationDashboard();
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
using YandexMusicApi.Network;

namespace YandexMusicApi.Tests.Api;

[TestFixture]
public class AccountTest : BaseCheck
{
    private ApiParams _apiParams;
    private YandexMusicApi.Api.Account _account;
    
    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        _apiParams = new ApiParams();
        _apiParams.TokenYandexMusic = ConfigurationRoot["API_TOKEN_YANDEX_MUSIC"];

        _account = new YandexMusicApi.Api.Account(_apiParams);
    }

    [Test]
    public async Task Expirements()
    {
        var result = await _account.Expirements();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task Promocode()
    {
        var result = await _account.Promocode("1231");
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task ShowSettings()
    {
        var result = await _account.ShowSettings();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task SettingsChanged()
    {
        Dictionary<string, string> data = new Dictionary<string, string>()
        { { "childModEnabled", "false" } };

        var result = await _account.SettingsChanged(data);
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task ShowInformAccount()
    {
        var result = await _account.ShowInformAccount();
        if (CheckIsFailed(result))
            Assert.Fail();
    }

    [Test]
    public async Task ShowBriefAccountInform()
    {
        var result = await _account.ShowBriefAccountInform();
        if (CheckIsFailed(result))
            Assert.Fail();
        else
            Console.WriteLine($"https://avatars.mds.yandex.net/get-yapic/{result["result"]["result"]["avatarId"]}/islands-middle");
    }

    [Test]
    public async Task ShowInformAccountFromYandexPassport()
    {
        var result = await _account.ShowInformAccountFromYandexPassport();
        if (CheckIsFailed(result))
            Assert.Fail();
    }
}
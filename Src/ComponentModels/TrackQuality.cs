using System.ComponentModel;

namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// Enum to specify the required track quality (lq, nq, lossless)
    /// /// </summary>
    public enum TrackQuality
    {
        /// <summary>
        /// Low track quality (lq)
        /// </summary>
        [Description("lq")] Low,
        /// <summary>
        /// Normal Track Quality (nq)
        /// </summary>
        [Description("nq")] Normal,
        /// <summary>
        /// High Track Quality (Lossless)
        /// </summary>
        [Description("lossless")] High
    }
}
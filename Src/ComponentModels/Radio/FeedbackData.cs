namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// The base class for feedback data. Includes required fields for derived classes.
    /// To use it in the Feedback method, create a list of <c>FeedbackData</c> and populate it with instances of derived classes like <c>RadioStartedFeedbackData</c>, <c>TrackStartedFeedbackData</c>, etc.
    /// </summary>
    public abstract class FeedbackData
    {
        /// <summary>
        /// Gets or sets the timestamp in UNIX format indicating when the feedback was created.
        /// Optional; if not provided, the current time can be set by the method automatically.
        /// </summary>
        public string TimeStamp { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackData"/> class with the specified feedback type.
        /// </summary>
        /// <param name="feedbackType">The type of feedback (e.g., <c>RadioStarted</c>, <c>Skip</c>, etc.).</param>
        protected FeedbackData(RadioEnums.FeedbackType feedbackType)
        {
            FeedbackType = feedbackType;
        }

        /// <summary>
        /// Gets the type of feedback. Once assigned, this value cannot be changed to prevent accidental modification,
        /// which could lead to inconsistencies (e.g., sending data meant for <c>RadioStarted</c> when <c>TrackStarted</c> was expected).
        /// </summary>
        public RadioEnums.FeedbackType FeedbackType { get; }
    }
}
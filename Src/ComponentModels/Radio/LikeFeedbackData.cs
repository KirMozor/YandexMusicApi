namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for a liked track in a radio session.
    /// This class contains information about the liked track and its associated metadata.
    /// </summary>
    public class LikeFeedbackData : FeedbackData
    {
        /// <summary>
        /// Gets or sets the unique ID for the radio. This ID is unique for each pack of tracks (there are 5 tracks in each pack).
        /// You can obtain the <c>BatchId</c> when you last executed the <c>Feedback</c> method or the <c>NewSession</c> method.
        /// </summary>
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the liked track.
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LikeFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.Like"/>.
        /// This type of feedback is used when a user has liked a track from a radio session.
        /// </summary>
        public LikeFeedbackData() : base(RadioEnums.FeedbackType.Like) { }
    }
}
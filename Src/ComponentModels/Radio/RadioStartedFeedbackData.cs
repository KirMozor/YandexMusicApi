namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for when the radio starts. Used when the user starts a radio session.
    /// </summary>
    public class RadioStartedFeedbackData : FeedbackData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioStartedFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.RadioStarted"/>.
        /// </summary>
        public RadioStartedFeedbackData() : base(RadioEnums.FeedbackType.RadioStarted) { }
    }
}
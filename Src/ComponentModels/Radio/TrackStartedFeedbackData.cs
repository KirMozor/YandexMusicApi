namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for when a user starts listening to a track.
    /// This type of feedback is used to indicate that the user has begun listening to a specific track in a radio session.
    /// </summary>
    public class TrackStartedFeedbackData : FeedbackData
    {
        /// <summary>
        /// Gets or sets the unique ID for the radio. This ID is unique for each pack of tracks (each pack contains 5 tracks).
        /// You can obtain the <c>BatchId</c> when you last executed the <c>Feedback</c> method or the <c>NewSession</c> method.
        /// </summary>
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the track that the user has started listening to.
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackStartedFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.TrackStarted"/>.
        /// </summary>
        public TrackStartedFeedbackData() : base(RadioEnums.FeedbackType.TrackStarted) { }
    }
}
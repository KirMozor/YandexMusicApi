namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for when a user removes a like from a track.
    /// </summary>
    public class UnlikeFeedbackData : FeedbackData
    {
        /// <summary>
        /// Gets or sets the unique ID for the radio. This ID is unique for each pack of tracks (each pack contains 5 tracks).
        /// Obtain the <c>BatchId</c> when you last executed the <c>Feedback</c> method or the <c>NewSession</c> method.
        /// </summary>
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the track from which the user removed the like.
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnlikeFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.Unlike"/>.
        /// </summary>
        public UnlikeFeedbackData() : base(RadioEnums.FeedbackType.Unlike) { }
    }
}
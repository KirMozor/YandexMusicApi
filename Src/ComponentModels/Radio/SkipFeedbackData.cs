namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for when a track is skipped. Used when the user skips a track in a radio session.
    /// </summary>
    public abstract class SkipFeedbackData : FeedbackData
    {
        /// <summary>
        /// Gets or sets the unique ID for the radio. This ID is unique for each pack of tracks (each pack contains 5 tracks).
        /// Obtain the <c>BatchId</c> when you last executed the <c>Feedback</c> method or the <c>NewSession</c> method.
        /// </summary>
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the track that was skipped.
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Gets or sets the duration for which the user listened to the track before skipping, in seconds.
        /// </summary>
        public float TotalPlayedSecond { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SkipFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.Skip"/>.
        /// </summary>
        public SkipFeedbackData() : base(RadioEnums.FeedbackType.Skip) { }
    }
}
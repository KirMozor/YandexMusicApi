using System.ComponentModel;

namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Enum collection for sending feedback to the radio
    /// </summary>
    public abstract class RadioEnums
    {
        /// <summary>
        /// Enum types for feedback in radio
        /// </summary>
        public enum FeedbackType
        {
            /// <summary>
            /// Send it when you start the radio
            /// </summary>
            [Description("radioStarted")] RadioStarted,

            /// <summary>
            /// Send it when the track started
            /// </summary>
            [Description("trackStarted")] TrackStarted,

            /// <summary>
            /// Send when the track is skipped
            /// </summary>
            [Description("skip")] Skip,

            /// <summary>
            /// Send when the track completes
            /// </summary>
            [Description("trackFinished")] TrackFinished,
            
            /// <summary>
            /// Send if the user has liked
            /// </summary>
            [Description("like")] Like,
            
            /// <summary>
            /// Send when the user has removed the like
            /// </summary>
            [Description("unlike")] Unlike
        }
    }
}
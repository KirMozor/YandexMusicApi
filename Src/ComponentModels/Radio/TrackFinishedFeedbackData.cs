namespace YandexMusicApi.ComponentModels.Radio
{
    /// <summary>
    /// Represents feedback data for when a track is finished. Used when the user finishes listening to a track.
    /// </summary>
    public class TrackFinishedFeedbackData : FeedbackData
    {
        /// <summary>
        /// Gets or sets the unique ID for the radio. This ID is unique for each pack of tracks (each pack contains 5 tracks).
        /// Obtain the <c>BatchId</c> when you last executed the <c>Feedback</c> method or the <c>NewSession</c> method.
        /// </summary>
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the track that was listened to until the end.
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Gets or sets the duration for which the user listened to the track, in seconds.
        /// </summary>
        public float TotalPlayedSecond { get; set; }

        /// <summary>
        /// Gets or sets the total length of the track, in seconds.
        /// </summary>
        public float TrackLengthSeconds { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackFinishedFeedbackData"/> class.
        /// Sets the <see cref="RadioEnums.FeedbackType"/> property to <see cref="RadioEnums.FeedbackType.TrackFinished"/>.
        /// </summary>
        public TrackFinishedFeedbackData() : base(RadioEnums.FeedbackType.TrackFinished) { }
    }
}
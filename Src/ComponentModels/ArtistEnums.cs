using System.ComponentModel;

namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// Enum for the Artist class
    /// </summary>
    public class ArtistEnums
    {
        /// <summary>
        /// Enum for sorting albums
        /// </summary>
        public enum SortBy
        {
            /// <summary>
            /// Sorting albums by rating
            /// </summary>
            [Description("rating")] Rating,

            /// <summary>
            /// Sort albums by year of release
            /// </summary>
            [Description("year")] Year,

            /// <summary>
            /// No sorting
            /// </summary>
            Default
        }
    }
}
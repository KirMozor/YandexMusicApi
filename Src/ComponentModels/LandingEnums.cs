using System.ComponentModel;

namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// Enums collection for requesting blocks from the landing page
    /// </summary>
    public class LandingEnums
    {
        /// <summary>
        /// Enum for block types in the landing page
        /// </summary>
        public enum BlockTypes
        {
            /// <summary>
            /// Returns the Style block. It recommends songs in the style of artists you often listen to
            /// </summary>
            [Description("in-style")] InStyle,

            /// <summary>
            /// The More Discoveries block returns. This contains the various My Wave settings with their names and IDs, for example: Under Classes -> My Wave, On the Road, its ID: activity:road-trip
            /// </summary>
            [Description("waves")] Waves,

            /// <summary>
            /// Block Met in My Wave. Here you can see the authors of the tracks you've met in My Wave
            /// </summary>
            [Description("personal-artists")] PersonalArtists,

            /// <summary>
            /// Block with your personalized playlists such as Premiere, Playlist of the Day and so on
            /// </summary>
            [Description("personal-playlists")] PersonalPlaylists,

            /// <summary>
            /// This is the recommended New Releases block. New albums and singles from your favorite artists are shown here
            /// </summary>
            [Description("new-releases")] NewReleases,

            /// <summary>
            /// A block of artists, playlists, albums and singles you last listened to
            /// </summary>
            [Description("recently-played")] RecentlyPlayed
        }

        /// <summary>
        /// Listing of block types on the landing page in the Editorial Promotion block
        /// </summary>
        public enum EditorialPromotionBlockTypes
        {
            /// <summary>
            /// Block with neuromusic generated by Yandex Music
            /// </summary>
            [Description("NEUROMUSIC")] Neuromusic,

            /// <summary>
            /// Crutch time clips. In human terms, it's a block with a list of clips you might like.
            /// </summary>
            [Description("kostyl_vremya_klipov")] TimeMusicalVideos
        }
    }
}
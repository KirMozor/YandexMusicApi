namespace YandexMusicApi.ComponentModels.SignRequest
{
    /// <summary>
    /// A class to obtain the Sign key, which is then applied in the <see cref="FetchTrackLinkRequest"/>
    /// </summary>
    public class FetchTrackLinkRequest
    {
        /// <summary>
        /// Track ID
        /// </summary>
        public int TrackId { get; set; }

        /// <summary>
        /// Track quality
        /// </summary>
        public string Quality { get; set; } = "lossless";
        
        /// <summary>
        /// Codec, while there are such options: aac, mp3, flac
        /// </summary>
        public string Codec { get; set; }

        /// <summary>
        /// Data transport method
        /// </summary>
        public string Transport { get; set; } = "encraw";
    }
}
namespace YandexMusicApi.ComponentModels.SignRequest
{
    /// <summary>
    /// Class to get the Sign key, it is applied when you get the lyrics of a song
    /// </summary>
    public class GetLyricRequest
    {
        /// <summary>
        /// ID трека
        /// </summary>
        public int TrackId { get; set; }
    }
}
using Newtonsoft.Json.Linq;

namespace YandexMusicApi.ComponentModels
{
    ///<summary>
    /// Represents a response object containing data returned from a Yandex login request.
    /// </summary>
    public class LoginResponse
    {
        /// <summary>
        /// Gets or sets the data returned from the login request.
        /// </summary>
        public JObject Data { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResponse"/> class with the specified data.
        /// </summary>
        /// <param name="data">The data returned from the login request.</param>
        public LoginResponse(JObject data)
        {
            Data = data;
        }
    }
}
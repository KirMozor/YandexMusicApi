namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// The model required for AddTracks. This is where the track ID and album ID of the track are specified
    /// </summary>
    public class TracksList
    {
        /// <summary>
        /// Track ID (kind)
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Album ID of the album your track is in
        /// </summary>
        public int? AlbumId { get; set; }
    }
}
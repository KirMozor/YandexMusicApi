namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// Model for Auth (needed for some requests, why? I don't know)
    /// </summary>
    public class SignModel
    {
        /// <summary>
        /// UNIX time
        /// </summary>
        public int Timestamp { get; set; }
        /// <summary>
        /// Auth key for some requests
        /// </summary>
        public string Value { get; set; }
    }
}
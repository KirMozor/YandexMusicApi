using System.ComponentModel;

namespace YandexMusicApi.ComponentModels
{
    /// <summary>
    /// Enums enumeration for the Search class
    /// </summary>
    public class SearchEnums
    {
        /// <summary>
        /// Фильтры для поиска в методе Search.RetrieveSearch
        /// </summary>
        public enum TypeSearch
        {
            /// <summary>
            /// Without filter
            /// </summary>
            [Description("all")] All,
            /// <summary>
            /// Looking for artists only
            /// </summary>
            [Description("artist")] Artists,
            /// <summary>
            /// Searching for tracks only
            /// </summary>
            [Description("track")] Tracks,
            /// <summary>
            /// Search albums only
            /// </summary>
            [Description("album")] Albums,
            /// <summary>
            /// Search only playlists
            /// </summary>
            [Description("playlist")] Playlists,
            /// <summary>
            /// Searching for podcasts only
            /// </summary>
            [Description("podcast")] Podcasts,
            /// <summary>
            /// Searching for audio-books only
            /// </summary>
            [Description("book")] Books,
            /// <summary>
            /// Searching for clips only (video)
            /// </summary>
            [Description("clip")] Clips,
        }
        /// <summary>
        /// Represents the types of items that can be included in the search history query.
        /// </summary>
        public enum SearchType
        {
            /// <summary>
            /// Represents all types of items. When used, all supported types will be included in the query.
            /// </summary>
            All,

            /// <summary>
            /// Represents a track item. Use this to include tracks in the search history query.
            /// </summary>
            Track,

            /// <summary>
            /// Represents a podcast episode item. Use this to include podcast episodes in the search history query.
            /// </summary>
            PodcastEpisode,

            /// <summary>
            /// Represents an album item. Use this to include albums in the search history query.
            /// </summary>
            Album,

            /// <summary>
            /// Represents a podcast item. Use this to include podcasts in the search history query.
            /// </summary>
            Podcast,

            /// <summary>
            /// Represents an artist item. Use this to include artists in the search history query.
            /// </summary>
            Artist,

            /// <summary>
            /// Represents a playlist item. Use this to include playlists in the search history query.
            /// </summary>
            Playlist,

            /// <summary>
            /// Represents a user-generated content track item. Use this to include user-generated content tracks in the search history query.
            /// </summary>
            UgcTrack,

            /// <summary>
            /// Represents a radio item. Use this to include radio in the search history query.
            /// </summary>
            Wave,

            /// <summary>
            /// Represents a clip item. Use this to include clips in the search history query.
            /// </summary>
            Clip
        }
    }
}
namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for an artist.
    /// </summary>
    /// <example>
    /// <code>
    /// var artistQueue = new ArtistQueue
    /// {
    ///     Description = "Artist Name",
    ///     Id = 67890,
    ///     TracksList = new List&lt;TracksList&gt;
    ///     {
    ///         new TracksList { Id = 1, AlbumId = 123 },
    ///         new TracksList { Id = 2, AlbumId = 456 }
    ///     }
    /// };
    /// </code>
    /// </example>
    public class ArtistQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArtistQueue"/> class with predefined parameters.
        /// </summary>
        public ArtistQueue() : base("mobile-artist-artist-default", "artist") { }

        /// <summary>
        /// Gets or sets the ID of the artist.
        /// </summary>
        public int Id { get; set; }
    }
}
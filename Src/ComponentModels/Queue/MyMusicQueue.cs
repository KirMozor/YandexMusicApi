namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for the user's own music.
    /// </summary>
    /// <example>
    /// <code>
    /// var myMusicQueue = new MyMusicQueue
    /// {
    ///     Description = "My Favorite Tracks",
    ///     UserId = "user123",
    ///     TracksList = new List&lt;TracksList&gt;
    ///     {
    ///         new TracksList { Id = 1, AlbumId = 789 },
    ///         new TracksList { Id = 2, AlbumId = 101 }
    ///     }
    /// };
    /// </code>
    /// </example>
    public class MyMusicQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyMusicQueue"/> class with predefined parameters.
        /// </summary>
        public MyMusicQueue() : base("mobile-own_tracks-playlist-default", "playlist") { }

        /// <summary>
        /// Gets the type of MyMusic queue. Always returns "my_music".
        /// </summary>
        public string TypeMyMusic { get; } = "my_music";

        /// <summary>
        /// Receives or sets the ID of the user who owns the My Music playlist
        /// </summary>
        public string UserId { get; set; }
    }
}
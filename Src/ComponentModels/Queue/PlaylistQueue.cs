namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for a playlist.
    /// </summary>
    /// <example>
    /// <code>
    /// var playlistQueue = new PlaylistQueue
    /// {
    ///     Description = "Playlist Title",
    ///     PlaylistId = 2710,
    ///     UserId = 103372440,
    ///     TracksList = new List&lt;TracksList&gt;
    ///
    ///         new TracksList { Id = 1, AlbumId = 789 },
    ///         new TracksList { Id = 2, AlbumId = 101 }
    ///     }
    /// };
    /// </code>
    /// </example>
    public class PlaylistQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlaylistQueue"/> class with predefined parameters.
        /// </summary>
        public PlaylistQueue() : base("mobile-user_playlists-playlist-default", "playlist") { }

        /// <summary>
        /// Gets or sets the ID of the playlist.
        /// </summary>
        public int PlaylistId { get; set; }

        /// <summary>
        /// Retrieves or sets the ID of the user who created the playlist.
        /// </summary>
        public int UserId { get; set; }
    }
}
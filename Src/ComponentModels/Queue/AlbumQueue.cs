using System.Collections.Generic;

namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for an album.
    /// </summary>
    /// <example>
    /// <code>
    /// var albumQueue = new AlbumQueue
    /// {
    ///     Description = "Album Title",
    ///     Id = 12345,
    ///     TracksList = new List&lt;TracksList&gt;
    ///     {
    ///         new TracksList { Id = 1, AlbumId = 12345 },
    ///         new TracksList { Id = 2, AlbumId = 12345 }
    ///     }
    /// };
    /// </code>
    /// </example>
    public class AlbumQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumQueue"/> class with predefined parameters.
        /// </summary>
        public AlbumQueue() : base("mobile-album-album-default", "album") { }

        /// <summary>
        /// Gets or sets the ID of the album.
        /// </summary>
        public int Id { get; set; }
    }
}
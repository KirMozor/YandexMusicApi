namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for tracks.
    /// </summary>
    /// <example>
    /// <code>
    /// var trackQueue = new TrackQueue
    /// {
    ///     Description = "Track Title",
    ///     TracksList = new List&lt;TracksList&gt;
    ///     {
    ///         new TracksList { Id = 42197229, AlbumId = 111 }
    ///     }
    /// };
    /// </code>
    /// </example>
    public class TrackQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrackQueue"/> class with predefined parameters.
        /// </summary>
        public TrackQueue() : base("mobile-track-track-default", "various") { }
    }
}
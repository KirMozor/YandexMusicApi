using System.Collections.Generic;

namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Base class representing the basic information required to create a queue.
    /// </summary>
    public abstract class QueueBaseData
    {
        private readonly string _from;
        private readonly string _type;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueueBaseData"/> class with specified parameters.
        /// </summary>
        /// <param name="from">The source of the queue.</param>
        /// <param name="type">The type of the queue.</param>
        protected QueueBaseData(string from, string type)
        {
            _from = from;
            _type = type;
        }

        /// <summary>
        /// Gets or sets the description of the queue. This is the name or label for the queue.
        /// </summary>
        /// <example>Skrillex (because you started tracks by this artist)</example>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the current index of the queue. Defaults to 0.
        /// </summary>
        public int CurrentIndex { get; set; } = 0;

        /// <summary>
        /// Gets or sets the list of tracks that will be included in the queue.
        /// </summary>
        public virtual List<TracksList> TracksList { get; set; }
    
        /// <summary>
        /// Gets the source or origin of the queue.
        /// </summary>
        public string From => _from;

        /// <summary>
        /// Gets the type of the queue.
        /// </summary>
        public string Type => _type;
    }
}
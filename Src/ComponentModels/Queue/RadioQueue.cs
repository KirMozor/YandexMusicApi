using System.Collections.Generic;

namespace YandexMusicApi.ComponentModels.Queue
{
    /// <summary>
    /// Represents a queue specifically for a radio session.
    /// </summary>
    /// <example>
    /// <code>
    /// var radioQueue = new RadioQueue
    /// {
    ///     Description = "Radio Wave",
    ///     Id = "user:onyourwave",
    ///     RadioSessionId = "session123"
    /// };
    /// </code>
    /// </example>
    public class RadioQueue : QueueBaseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RadioQueue"/> class with predefined parameters.
        /// </summary>
        public RadioQueue() : base("radio-mobile-home-discovery_block-waves-default", "radio") { }

        /// <summary>
        /// Gets or sets the radio session ID.
        /// </summary>
        public string RadioSessionId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the radio queue.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Override for TracksList. Radio queues do not use this property. Don't changed
        /// </summary>
        public override List<TracksList> TracksList
        {
            get => null;
            set { /* do nothing */ }
        }
    }
}
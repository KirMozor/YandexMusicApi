using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.ComponentModels.Queue;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// Provides methods for interacting with Yandex Music queues, including creating, retrieving, and updating queues.
    /// </summary>
    public class Queue : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and NetworkParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, language and UserAgent installed</param>
        public Queue(ApiParams apiParams) : base(apiParams) { }
        
        /// <summary>
        /// Getting the current queue
        /// </summary>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Queue ID, queue type and when modified</returns>
        public async Task<JObject> GetQueue()
        {
            CheckToken();
            var result = await PostGet.GetAsync($"queues");
            return result;
        }
        /// <summary>
        /// Getting queue by ID with tracks in it
        /// </summary>
        /// <param name="queueId">Queue ID, can be obtained from the GetQueue method</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Queue description, queue type (playlist, album, etc.) and tracks in the queue</returns>
        public async Task<JObject> GetQueueById(string queueId)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"queues/{queueId}");
            return result;
        }
        
        /// <summary>
        /// Updating the queue. When a new track is enabled, the index of the track in the queue is passed (the current queue can be obtained from the GetQueue method)
        /// </summary>
        /// <param name="queueId">ID of the current queue (you can also get everything from the GetQueue method)</param>
        /// <param name="currentIndex">Index of the track that is currently playing</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>OK if you managed to update, or an error if the queue ID is incorrect or you have gone beyond the queue boundaries (Index out of range)</returns>
        public async Task<JObject> UpdateQueue(string queueId, int currentIndex)
        {
            CheckToken();
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "isInteractive", "false" }
            };
            var result = await PostGet.PostAsync($"queues/{queueId}/update-position?currentIndex={currentIndex}", new FormUrlEncodedContent(data));
            return result;
        }

        /// <summary>
        /// Creates a queue for tracks, artists, albums, playlists, my music, or radio stations.
        /// </summary>
        /// <typeparam name="T">The type of queue data, which must inherit from <see cref="QueueBaseData"/>. Allowed types are:
        /// <see cref="TrackQueue"/>, <see cref="ArtistQueue"/>, <see cref="AlbumQueue"/>, <see cref="PlaylistQueue"/>,
        /// <see cref="MyMusicQueue"/>, <see cref="RadioQueue"/>.</typeparam>
        /// <param name="queueData">The queue data object containing information about the queue type and additional data.</param>
        /// <returns>Returns a <see cref="Task{JObject}"/> - a JSON object containing information about the created queue,
        /// including the queue ID.</returns>
        /// <exception cref="ArgumentException">Thrown if the provided <paramref name="queueData"/> object is not an instance
        /// of one of the supported types: <see cref="TrackQueue"/>, <see cref="ArtistQueue"/>, <see cref="AlbumQueue"/>,
        /// <see cref="PlaylistQueue"/>, <see cref="MyMusicQueue"/>, <see cref="RadioQueue"/>.</exception>
        /// <remarks>
        /// Do not include <see cref="TracksList"/> in <see cref="RadioQueue"/>.
        /// </remarks>
        public async Task<JObject> CreateQueue<T>(T queueData) where T : QueueBaseData
        {
            var context = new JObject()
            {
                ["description"] = queueData.Description,
                ["type"] = queueData.Type
            };
            
            var initialContent = new JObject()
            {
                ["type"] = queueData.Type
            };
            
            JArray tracks = null;
            if (!(queueData is RadioQueue))
            {
                tracks = new JArray(
                    queueData.TracksList.Select(track => new JObject
                    {
                        ["albumId"] = track.AlbumId,
                        ["from"] = queueData.From,
                        ["trackId"] = track.Id
                    })
                );
            }
            
            var requestBody = new JObject
            {
                ["context"] = context,
                ["currentIndex"] = queueData.CurrentIndex,
                ["initialContext"] = initialContent,
                ["isInteractive"] = true
            };
            
            if (tracks != null)
                requestBody["tracks"] = tracks;

            switch (queueData)
            {
                case TrackQueue _:
                    break;
                case ArtistQueue artistQueue:
                    context.Add("id", artistQueue.Id);
                    initialContent.Add("id", artistQueue.Id);
                    break;
                case AlbumQueue albumQueue:
                    context.Add("id", albumQueue.Id);
                    initialContent.Add("id", albumQueue.Id);
                    break;
                case PlaylistQueue playlistQueue:
                    context.Add("id", $"{playlistQueue.UserId}:{playlistQueue.PlaylistId}");
                    initialContent.Add("id", $"{playlistQueue.UserId}:{playlistQueue.PlaylistId}");
                    break;
                case MyMusicQueue myMusicQueue:
                    context.Add("id", $"{myMusicQueue.UserId}:3");
                    initialContent.Add("id", $"{myMusicQueue.UserId}:3");
                    
                    context["type"] = myMusicQueue.TypeMyMusic;
                    break;
                case RadioQueue radioQueue:
                    var options = new JObject
                    {
                        ["radioOptions"] = new JObject
                        {
                            ["sessionId"] = radioQueue.RadioSessionId
                        }
                    };
                    requestBody["from"] = radioQueue.From;
                    context.Add("options", options);
                    initialContent.Add("options", options);
                    
                    context.Add("id", radioQueue.Id);
                    initialContent.Add("id", radioQueue.Id);
                    break;
                default:
                    throw new ArgumentException("You passed the wrong class");
            }
            
            var content = new StringContent(requestBody.ToString(), System.Text.Encoding.UTF8, "application/json");
            return await PostGet.PostAsync("queues", content);
        }
    }
}
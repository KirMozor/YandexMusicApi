using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is used to retrieve tracks from an album, get album information, like/unlike albums, get albums you like
    /// </summary>
    public class Album : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Album(ApiParams apiParams) : base(apiParams) { }
        /// <summary>
        /// Displays information about the album
        /// </summary>
        /// <param name="albumId">Album ID of the album you want to get information about</param>
        /// <returns>Album type, year of release, album photo link, genre, who released, number of tracks, and so on</returns>
        public async Task<JObject> InformAlbum(int albumId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };

            var result = await PostGet.GetAsync($"albums/{albumId}", header);
            return result;
        }

        /// <summary>
        /// Displays all the tracks on the album
        /// </summary>
        /// <returns>Get the same information as in InformAlbum but with tracks from an album</returns>
        /// <param name="albumId">Album ID of the album you want to get information about</param>
        /// <param name="page">Track list page (Optional, default 0)</param>
        /// <param name="pageSize">Track output page size (Optional, default 10)</param>
        public async Task<JObject> GetTracks(int albumId, int page = 0, int pageSize = 10)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" },
                { "page", page.ToString() },
                { "pageSize", pageSize.ToString() }
            };

            var result = await PostGet.GetAsync($"albums/{albumId}/with-tracks", header);
            return result;
        }

        /// <summary>
        /// Displaying information about multiple albums
        /// </summary>
        /// <returns>Returns information about albums, basically the same as InformAlbum, but you can get information about many albums at once</returns>
        /// <param name="albumIds">The album IDs you want to get information about</param>
        public async Task<JObject> InformAlbums(List<int> albumIds)
        {
            var headers = new Dictionary<string, string> { { "Accept", "application/json" } };

            string albumIdsString = string.Join(",", albumIds);
            var data = new Dictionary<string, string> { { "album-ids", albumIdsString } };

            var result = await PostGet.PostAsync("albums", new FormUrlEncodedContent(data), headers);
            return result;
        }

        /// <summary>
        /// Add albums to favorites (liked)
        /// </summary>
        /// <returns>Returns OK if successful</returns>
        /// <param name="likeAlbums">List of albums that need to be liked</param>
        /// <param name="userId">Profile through which you need to like albums (they will be in his favorites) (add the uid of the user through which you work)</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> LikesAlbums(List<int> likeAlbums, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };

            string likeTracksIdString = string.Join(",", likeAlbums);
            Dictionary<string, string> data = new Dictionary<string, string>() { { "album-ids", likeTracksIdString } };

            var result = await PostGet.PostAsync($"users/{userId}/likes/albums/add-multiple", new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Get the user's favorite (liked) albums
        /// </summary>
        /// <param name="userId">The profile through which you need to view the liked albums (add the uid of the user through which you work)</param>
        /// <returns>List of user's favorite albums</returns>
        public async Task<JObject> GetLikesAlbum(string userId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };

            var result = await PostGet.GetAsync($"users/{userId}/likes/albums", header);
            return result;
        }

        /// <summary>
        /// Remove albums from favorites (likes)
        /// </summary>
        /// <returns>Returns OK if successful</returns>
        /// <param name="likeAlbums">List of albums to remove from like</param>
        /// <param name="userId">Profile through which you want to remove albums from your favorites (add the uid of the user you are working through)</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> RemoveLikesAlbums(List<int> likeAlbums, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };

            string removeTracksIdString = string.Join(",", likeAlbums);
            Dictionary<string, string> data = new Dictionary<string, string>() { { "album-ids", removeTracksIdString } };

            var result = await PostGet.PostAsync($"users/{userId}/likes/albums/remove", new FormUrlEncodedContent(data), header);
            return result;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is used to get an artist's tracks, get information about the artist, like/unlike the artist, get the artists you like
    /// </summary>
    public class Artist : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Artist(ApiParams apiParams) : base(apiParams) { }

        /// <summary>
        /// Returns the artist's tracks
        /// </summary>
        /// <returns>List of artist's tracks with information</returns>
        /// <param name="artistId">The ID of the artist whose tracks you want to get</param>
        /// <param name="page">Track list page (Optional, default 0)</param>
        /// <param name="pageSize">Track output page size (Optional, default 10)</param>
        /// <param name="sortByRating">Sort tracks by rating (false by default)</param>
        public async Task<JObject> GetTracks(int artistId, int page = 0, int pageSize = 10, bool sortByRating = false)
        {
            string urlToRequest = $"artists/{artistId}/tracks?page={page}&page-size={pageSize}";
            if (sortByRating)
                urlToRequest = $"artists/{artistId}/track-ids-by-rating?page={page}&page-size={pageSize}";

            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "*/*" }
            };
            var result = await PostGet.GetAsync(urlToRequest, header);
            return result;
        }

        /// <summary>
        /// Getting information about the artist and news. As on this page: https://music.yandex.kz/artist/675068/info
        /// </summary>
        /// <returns>Information about the artist, links to social networks, artist rating, number of tracks, genre, similar artists, playlists, my wave by artist, etc.</returns>
        /// <param name="artistId">Artist ID from which you want to get information</param>
        public async Task<JObject> GetBriefInfo(int artistId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "*/*" }
            };

            var result = await PostGet.GetAsync($"artists/{artistId}/brief-info", header);
            return result;
        }

        /// <summary>
        /// Getting all of the artist's albums
        /// </summary>
        /// <returns>A list of the artist's albums with information about the album itself, album ID and so on</returns>
        /// <param name="artistId">The ID of the artist whose albums you want to get</param>
        /// <param name="page">Albums issue page (optional)</param>
        /// <param name="pageSize">Page size(optional)</param>
        /// <param name="sortBy">Sort the output. You can sort by year "year" or by rating "rating". You can sort without sorting "--" (optional)</param>
        public async Task<JObject> GetAlbums(int artistId, int page = 0, int pageSize = 10,
            ArtistEnums.SortBy sortBy = ArtistEnums.SortBy.Default)
        {
            string urlToRequest = $"artists/{artistId}/direct-albums?page={page}&page-size={pageSize}";
            if (sortBy != ArtistEnums.SortBy.Default)
                urlToRequest += $"&sort-by={GetEnumDescription(sortBy)}";

            var headers = new Dictionary<string, string>
            {
                { "accept", "*/*" }
            };

            var result = await PostGet.GetAsync(urlToRequest, headers);
            return result;
        }

        /// <summary>
        /// Getting all the information about the performer (e.g: All albums, popular tracks, artist genre, Waves by artist)
        /// </summary>
        /// <returns>Full artist information, band name, popular tracks, all albums, wave by artist, genre, similar artists, artist background video and so on</returns>
        /// <param name="artistId">Artist ID</param>
        public async Task<JObject> InformArtist(int artistId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "*/*" }
            };
            var result = await PostGet.GetAsync($"artists/{artistId}", header);
            return result;
        }

        /// <summary>
        /// Like the artists on the list
        /// </summary>
        /// <param name="likeArtists">List of artists as IDs</param>
        /// <param name="userId">The ID of the user who needs to add these artists to favorites (insert the ID to which the token belongs)</param>
        /// <returns>OK if you managed to add an artist to your favorites</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> LikesArtists(List<int> likeArtists, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            string likeArtistsIds = string.Join(",", likeArtists);
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "artist-ids", likeArtistsIds }
            };

            var result = await PostGet.PostAsync($"users/{userId}/likes/artists/add-multiple",
                new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Getting performers the user likes
        /// </summary>
        /// <param name="userId">The ID of the user you want</param>
        /// <returns>List of artists the user likes with information about them</returns>
        public async Task<JObject> GetLikesArtist(string userId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            var result = await PostGet.GetAsync($"users/{userId}/likes/artists", header);
            return result;
        }

        /// <summary>
        /// Removing artists from favorites
        /// </summary>
        /// <param name="likeArtists">List of artists as IDs that need to be deleted</param>
        /// <param name="userId">The ID of the user to remove these artists from favorites (insert the ID to which the token belongs)</param>.
        /// <returns>OK if you succeed in deleting an artist from your favorites</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> RemoveLikesArtists(List<int> likeArtists, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            string removeTracksIdString = string.Join(",", likeArtists);
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "artist-ids", removeTracksIdString }
            };

            var result = await PostGet.PostAsync($"users/{userId}/likes/artists/remove",
                new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Number of artist tracks you've listened to from my wave
        /// </summary>
        /// <param name="artistId">Artist ID</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        /// <returns>Number of tracks</returns>
        public async Task<JObject> FamiliarYou(int artistId)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"artists/{artistId}/familiar-you/info?withWaveInfo=true");
            return result;
        }
    }
}
using System;
using System.ComponentModel;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// The base class from which all methods are inherited, here the internal PostGet class is initialised to make requests, the token is inserted and network parameters are applied
    /// </summary>
    public class BaseApi
    {
        private readonly ApiParams _apiParams;
        internal readonly PostGet PostGet;

        /// <summary>
        /// Basic constructor where PostGet is initialised with the given network parameters and, if there is a token, also inserted in requests to API methods
        /// </summary>
        /// <param name="apiParams">ApiParams class object with network parameters</param>
        protected BaseApi(ApiParams apiParams)
        {
            _apiParams = apiParams;
            PostGet = new PostGet(apiParams);
        }
        /// <summary>
        /// Returns a description of the enumeration value using the Description attribute.
        /// </summary>
        /// <param name="value">The meaning of enumeration.</param>
        /// <returns>A description of the enumeration value or the value itself if the Description attribute is not found.</returns>
        protected string GetEnumDescription(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attribute = (DescriptionAttribute)Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute));
            return attribute == null ? value.ToString() : attribute.Description;
        }
        /// <summary>
        /// Returns an ArgumentException error if no token is available, applicable in some cases when a token is required
        /// </summary>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        protected void CheckToken()
        {
            if (_apiParams.TokenYandexMusic == null)
                throw new ArgumentException("Token is null");
        }
    }
}
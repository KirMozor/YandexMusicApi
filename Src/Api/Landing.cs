using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is responsible for retrieving information from the YandexMusic landing page, i.e. the home screen
    /// </summary>
    public class Landing : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Landing(ApiParams apiParams) : base(apiParams) { }

        /// <summary>
        /// Getting the skeleton (mockup) of the landing page, how the playlists, my wave and other elements of the landing page are arranged
        /// </summary>
        /// <param name="showWizard">I haven't understood exactly what this parameter is responsible for, but the value false (by default) allows you to get more information</param>
        /// <returns>JObject object with the skeleton (layout) of the Yandex Music main page</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> GetSkeleton(bool showWizard = false)
        {
            CheckToken();   
            var result = await PostGet.GetAsync($"landing/skeleton/main?showWizard={showWizard}");
            return result;
        }

        /// <summary>
        /// Gets information about a block of landings.
        /// </summary>
        /// <param name="landingBlockTypes">Landing block type</param>
        /// <returns>JObject object with information about the block.</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> GetBlock(LandingEnums.BlockTypes landingBlockTypes)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"landing/block/{GetEnumDescription(landingBlockTypes)}");
            return result;
        }

        /// <summary>
        /// Getting information from editorial promotion blocks (what is that anyway?) blocks from the landing page. For example neuromusic or clip time
        /// </summary>
        /// <param name="landingEditorialPromotionBlockTypes">Type of editorial promotion block</param>
        /// <returns>JObject object with information about promotion blocks</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> GetEditorialPromotionBlock(LandingEnums.EditorialPromotionBlockTypes landingEditorialPromotionBlockTypes)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"landing/block/editorial-promotion/{GetEnumDescription(landingEditorialPromotionBlockTypes)}");
            return result;
        }
        /// <summary>
        /// List of radio stations from the home page
        /// </summary>
        /// <returns>Returns a JObject with the query result. In asynchronous form</returns>
        public async Task<JObject> StationDashboard()
        {
            var result = await PostGet.GetAsync($"rotor/stations/dashboard");
            return result;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// A class for working with playlists
    /// </summary>
    public class Playlist : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Playlist(ApiParams apiParams) : base(apiParams) {}

        /// <summary>
        /// Returns the list of playlists that the user liked
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>List of playlists that the user has liked</returns>
        public async Task<JObject> GetLikesPlaylists(string userId)
        {
            var result = await PostGet.GetAsync($"users/{userId}/likes/playlists");
            return result;
        }
        /// <summary>
        /// Allows you to change the name of your playlist (provided you are the owner of the playlist)
        /// </summary>
        /// <param name="userId">ID of the user who owns the playlist</param>
        /// <param name="playlistKind">ID (kind) of the playlist you want to change the name of</param>
        /// <param name="newNamePlaylist">New playlist name</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Playlist information if everything is OK</returns>
        public async Task<JObject> ChangeNamePlaylist(string userId, int playlistKind, string newNamePlaylist)
        {
            CheckToken();
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                {"value", newNamePlaylist}
            };
            var result = await PostGet.PostAsync($"users/{userId}/playlists/{playlistKind}/name", 
                new FormUrlEncodedContent(data));
            return result;
        }

        /// <summary>
        /// Creating a custom playlist
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="namePlaylist">New playlist name</param>
        /// <param name="isVisible">Playlist visibility to other users, default false (private)</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>The new playlist ID, name, creation date, and owner are returned</returns>
        public async Task<JObject> CreatePlaylist(string userId, string namePlaylist, bool isVisible = false)
        {
            CheckToken();
            string visibility = isVisible ? "public" : "private";
            
            var result = await PostGet.PostAsync($"users/{userId}/playlists/create?title={namePlaylist}&visibility={visibility}");
            return result;
        }

        /// <summary>
        /// Returns a list of user's playlists (may return an empty list if no token is passed)
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>Returns the user's playlist, playlist name, playlist uid and kind, playlist picture and other information</returns>
        public async Task<JObject> ListUserPlaylists(string userId)
        {
            var result = await PostGet.GetAsync($"users/{userId}/playlists/list");
            return result;
        }

        /// <summary>
        /// Deletes a user's playlist by its kind
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="kindPlaylist">Playlist ID (kind)</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Returns JSON that everything is ok, or an error if the data is not specified correctly</returns>
        public async Task<JObject> DeletePlaylist(string userId, int kindPlaylist)
        {
            CheckToken();
            var result = await PostGet.PostAsync($"users/{userId}/playlists/{kindPlaylist}/delete");
            return result;
        }
        
        /// <summary>
        /// Get all of the information about the playlist (including the track list). If the playlist is private and you don't have access (or the request is made without a token), there will be a NotFound error in status
        /// </summary>
        /// <param name="userId">User ID of the user who owns the playlist (uid)</param>
        /// <param name="kind">Playlist ID (kind)</param>
        /// <param name="page">Page of information output (default is 0) (optional)</param>
        /// <param name="pageSize">The size of the output page (default 10) (optional)</param>
        /// <returns>returns a JObject with the query result. In asynchronous form</returns>
        public async Task<JObject> InformPlaylistAndTracks(string userId, int kind, int page = 0, int pageSize = 10)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };
            var result = await PostGet.GetAsync($"users/{userId}/playlists/{kind}?page={page}&page-size={pageSize}", header);
            return result;
        }

        /// <summary>
        /// Adds a list of tracks to the playlist you need
        /// </summary>
        /// <param name="userId">ID of the user who owns the playlist</param>
        /// <param name="idPlaylist">ID of the playlist where you want to add tracks</param>
        /// <param name="listTracks">List of tracks to be added</param>
        /// <param name="revision">The number of changes in the playlist, you can get this number by calling the InformPlaylist method and passing the ID of the playlist you need there</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Information about the playlist, its revision, who created it and so on</returns>
        public async Task<JObject> AddTracks(string userId, int idPlaylist, List<TracksList> listTracks, int revision)
        {
            return await AddRemoveTracks(userId, idPlaylist, listTracks, revision, true);
        }
        
        /// <summary>
        /// Removes the track list from the playlist you want to play
        /// </summary>
        /// <param name="userId">ID of the user who owns the playlist</param>
        /// <param name="idPlaylist">ID of the playlist where you want to remove tracks</param>
        /// <param name="listTracks">List of tracks to be removing</param>
        /// <param name="revision">The number of changes in the playlist, you can get this number by calling the InformPlaylist method and passing the ID of the playlist you need there</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Information about the playlist, its revision, who created it and so on</returns>
        public async Task<JObject> RemoveTracks(string userId, int idPlaylist, List<TracksList> listTracks, int revision)
        {
            return await AddRemoveTracks(userId, idPlaylist, listTracks, revision, false);
        }
        
        /// <summary>
        /// Private method for adding and removing tracks to a playlist
        /// </summary>
        /// <param name="userId">ID of the user who owns the playlist</param>
        /// <param name="idPlaylist">ID of the playlist where you want to add tracks or remove</param>
        /// <param name="listTracks">List of tracks to be added or remove</param>
        /// <param name="revision">The number of changes in the playlist, you can get this number by calling the InformPlaylist method and passing the ID of the playlist you need there</param>
        /// <param name="addOrRemove">If true, tracks will be added, false will be deleted</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Information about the playlist, its revision, who created it and so on</returns>
        private async Task<JObject> AddRemoveTracks(string userId, int idPlaylist, List<TracksList> listTracks, int revision, bool addOrRemove)
        {
            CheckToken();
            var operation = addOrRemove ? "insert" : "delete";
            var diffObject = new JArray
            {
                new JObject
                {
                    { "op", operation },
                    { "to", 1 },
                    { addOrRemove ? "at" : "from", 0 },
                    { "tracks", JArray.FromObject(listTracks.Select(track => new JObject { { "id", track.Id }, { "albumId", track.AlbumId } })) }
                }
            };

            var data = new Dictionary<string, string>
            {
                { "diff", diffObject.ToString(Formatting.None) }
            };

            var result = await PostGet.PostAsync($"users/{userId}/playlists/{idPlaylist}/change-relative?revision={revision}",
                new FormUrlEncodedContent(data));
            return result;
        }
        
        /// <summary>
        /// Getting playlist recommendations
        /// </summary>
        /// <param name="userId">ID user</param>
        /// <param name="idPlaylist">Playlist ID (kind)</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>List of recommendations</returns>
        public async Task<JObject> GetRecommendations(string userId, int idPlaylist)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"users/{userId}/playlists/{idPlaylist}/recommendations");
            return result;
        }

        /// <summary>
        /// Change the visibility of a playlist to other people
        /// </summary>
        /// <param name="userId">ID of the user who owns the playlist</param>
        /// <param name="idPlaylist">Playlist ID to change visibility</param>
        /// <param name="visibility">true to make the playlist public, false to make it private</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Information about the playlist and whether the playlist is public or not</returns>
        public async Task<JObject> ChangeVisibility(string userId, int idPlaylist, bool visibility)
        {
            CheckToken();
            var data = new Dictionary<string, string>()
            {
                { "value", visibility ? "public" : "private" }
            };
            var result = await PostGet.PostAsync($"users/{userId}/playlists/{idPlaylist}/visibility", new FormUrlEncodedContent(data));
            return result;
        }
    }
}
using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// A class to work with search, delete history, search with sorting
    /// </summary>
    public class Search : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Search(ApiParams apiParams) : base(apiParams) { }
        
        /// <summary>
        /// Retrieves user search history.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="types">The types of items to include in the search history. Use <see cref="SearchEnums.SearchType.All"/> to include all types.</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Listen history as a list with type, ID, etc.</returns>
        public async Task<JObject> GetSearchHistory(string userId, params SearchEnums.SearchType[] types)
        {
            CheckToken();
            var supportedTypes = ToQueryString(types);
            var result = await PostGet.GetAsync($"users/{userId}/search-history?supportedTypes={supportedTypes}");
            return result;
        }
        
        /// <summary>
        /// Clears the user's search history
        /// </summary>
        /// <param name="userId">user ID</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        /// <returns>OK if cleared</returns>
        public async Task<JObject> ClearSearchHistory(string userId)
        {
            CheckToken();
            var result = await PostGet.GetAsync($"users/{userId}/search-history/clear");
            return result;
        }
        
        /// <summary>
        /// Search on Yandex Music
        /// </summary>
        /// <param name="searchQuery">Search query</param>
        /// <param name="noCorrect">Whether to correct the query in case of errors</param>
        /// <param name="page">Issue page</param>
        /// <param name="pageSize">Number of renditions per search query per page</param>
        /// <param name="typeSearch">Search Filter</param>
        /// <returns>Search result :D</returns>
        public async Task<JObject> RetrieveSearch(string searchQuery, bool noCorrect = false, int page = 0, int pageSize = 20, SearchEnums.TypeSearch typeSearch = SearchEnums.TypeSearch.All)
        {
            string type = GetEnumDescription(typeSearch);
            var result =
                await PostGet.GetAsync(
                    $"search/instant/mixed?withLikesCount=true&text={searchQuery}&nocorrect={noCorrect}&inputType=keyboard&page={page}&pageSize={pageSize}&type={type}");
            return result;
        }
        
        /// <summary>
        /// Converts an <see cref="SearchEnums.SearchType"/> or a collection of <see cref="SearchEnums.SearchType"/> to a query string parameter for supported types.
        /// </summary>
        /// <param name="types">The types to include in the query parameter. If <c>SearchType.All</c> is included, all types will be used.</param>
        /// <returns>A query string parameter representing the supported types.</returns>
        private string ToQueryString(params SearchEnums.SearchType[] types)
        {
            if (types.Contains(SearchEnums.SearchType.All))
            {
                return "all";
            }

            var typeStrings = types.Select(t => t.ToString().Replace('_', ' ').ToLower());
            return string.Join(",", typeStrings);
        }
    }
}
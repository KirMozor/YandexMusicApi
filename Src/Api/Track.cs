using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.ComponentModels.SignRequest;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is for getting tracks you like, removing tracks you like, getting track information, downloading tracks, getting similar tracks and getting track lyrics
    /// </summary>
    public class Track : BaseApi
    {
        private Dictionary<string, string> _downloadInfo = new Dictionary<string, string>();
        private const string DefaultSignKey = "p93jhgh689SBReK6ghtw62";
        private const string FetchTrackLinkSignKey = "kzqU4XhfCaY6B6JTHODeq5";
        
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Track(ApiParams apiParams) : base(apiParams) { }

        /// <summary>
        /// Adding a list of tracks to favorites (likes)
        /// </summary>
        /// <param name="likeTracks">List of tracks as IDs to add to favorites</param>
        /// <param name="userId">The ID of the user to add tracks to favorites (also use the name of the user who owns the token)</param>.
        /// <returns>Returns some kind of revision, the tracks themselves are added to favorites</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> AddLikesTracks(List<int> likeTracks, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            string likeTracksIdString = string.Join(",", likeTracks);
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "track-ids", likeTracksIdString }
            };

            var result = await PostGet.PostAsync($"users/{userId}/likes/tracks/add-multiple", new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Deleting a track list from favorites (unlike)
        /// </summary>
        /// <param name="likeTracks">List of tracks as IDs to remove from favorites</param>
        /// <param name="userId">The ID of the user to delete the tracks (also use the name of the user who owns the token)</param>.
        /// <returns>Returns some kind of revision, and the tracks themselves are removed from favorites</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> RemoveLikesTracks(List<int> likeTracks, string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            string removeTracksIdString = string.Join(",", likeTracks);
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "track-ids", removeTracksIdString }
            };

            var result = await PostGet.PostAsync($"users/{userId}/likes/tracks/remove", new FormUrlEncodedContent(data), header);
            return result;
        }
        
        /// <summary>
        /// Getting a list of your favorite tracks
        /// </summary>
        /// <param name="userId">The ID of the user from whom you want to get the list of tracks you like</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>Returns a list of tracks from your favorites</returns>
        public async Task<JObject> GetLikesTrack(string userId)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() {
                { "accept", "application/json" }
            };
            
            var result = await PostGet.GetAsync($"users/{userId}/likes/tracks", header);
            return result;
        }

        /// <summary>
        /// Retrieve information about the track (its title, the artist who wrote it, the album the track belongs to)
        /// </summary>
        /// <param name="idTracks">List of tracks as IDs about which you want to get information</param>
        /// <returns>Returns information about the track, who the author is, what album it's on, whether there are lyrics, and so on</returns>
        public async Task<JObject> GetInformTrack(List<int> idTracks)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            string tracksIdString = string.Join(",", idTracks);
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "trackIds", tracksIdString }
            };
            
            var result = await PostGet.PostAsync($"tracks?with-positions=true", new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Getting a list of formats in which you can get the track, its bitrate and downloadInfoUrl (it is necessary to get a direct link to the track). If the token is specified in the class constructor, you will get the full track, if not, you will get a 30-second segment.
        /// </summary>
        /// <param name="trackId">Track ID</param>
        /// <returns>A list of available song formats, track bitrate and downloadInfoUrl to get a direct link</returns>
        /// <remarks>
        /// <b>Deprecated.</b> This method is deprecated and will be removed in future versions. 
        /// Use <see cref="FetchTrackLink"/> instead.
        /// </remarks>
        [Obsolete("This method is deprecated and will be removed in future versions. Use FetchTrackLink instead.")]
        public async Task<JObject> GetDownloadInfo(int trackId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            var result = await PostGet.GetAsync($"tracks/{trackId}/download-info", header);
            _downloadInfo = new Dictionary<string, string>();
            
            if (result["status"].ToString() == "OK")
                foreach (var item in result["result"]["result"])
                {
                    string downloadInfoUrl = item["downloadInfoUrl"].ToString();
                    string codec = item["codec"].ToString();

                    _downloadInfo.Add(downloadInfoUrl, codec);
                }

            return result;
        }
        
        /// <summary>
        /// This method returns a direct reference to the track (<see cref="GetDirectLink"/> and <see cref="GetDownloadInfo"/> no longer needed)/>
        /// </summary>
        /// <param name="trackId">Track ID</param>
        /// <param name="trackQuality">The quality of the track you need is low (lq), normal (nq) and high (lossless)</param>
        /// <returns>Direct link to the track (ready to listen), codec and bitrate</returns>
        public async Task<JObject> FetchTrackLink(int trackId, TrackQuality trackQuality)
        {
            string transport = "raw";
            string codecs = "flac,aac,he-aac,mp3";
            
            var signKey = GetSingKey(new FetchTrackLinkRequest
            {
                Codec = codecs,
                Quality = GetEnumDescription(trackQuality),
                TrackId = trackId,
                Transport = transport
            });
            
            var result =
                await PostGet.GetAsync(
                    $"get-file-info?ts={signKey.Timestamp}&trackId={trackId}&quality={GetEnumDescription(trackQuality)}&codecs={codecs}&transports={transport}&sign={signKey.Value}");
            return result;
        }

        /// <summary>
        /// Internal method to retrieve the XML to get the full track link
        /// </summary>
        /// <param name="xml">XML as a string</param>
        /// <returns>The dictionary, the result of parsing, is returned</returns>
        private static Dictionary<string, string> GetXml(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            IEnumerable<XElement> el = doc.Root.Elements();
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (XElement i in el) result.Add(i.Name.ToString(), i.Value);
            return result;
        }

        /// <summary>
        /// This method returns a direct link to the track
        /// </summary>
        /// <param name="downloadInfoUrl">Here you need to pass the url (downloadInfoUrl) that you got from the GetDownloadInfo method</param>
        /// <returns>Returns a string with a direct link</returns>.
        /// <remarks>
        /// <b>Deprecated.</b> This method is deprecated and will be removed in future versions. 
        /// Use <see cref="FetchTrackLink"/> instead.
        /// </remarks>
        [Obsolete("This method is deprecated and will be removed in future versions. Use FetchTrackLink instead.")]
        public async Task<string> GetDirectLink(string downloadInfoUrl)
        {
            var resultGetXml = await PostGet.GetAsync(downloadInfoUrl, ifConvertToJson: false);
            Dictionary<string, string> xmlResult = GetXml(resultGetXml["result"].ToString());

            string host = xmlResult["host"];
            string path = xmlResult["path"];
            string ts = xmlResult["ts"];
            string s = xmlResult["s"];

            string secret = $"XGRlBW9FXlekgbPrRHuSiA{path.Substring(1, path.Length - 1)}{s}";
            MD5 md5 = MD5.Create();
            byte[] md5Hash = md5.ComputeHash(Encoding.UTF8.GetBytes(secret));
            HMACSHA1 hmacSha1 = new HMACSHA1(); 
            byte[] hmaSha1Hash = hmacSha1.ComputeHash(md5Hash);
            string sign = BitConverter.ToString(hmaSha1Hash).Replace("-", "").ToLower();

            return $"https://{host}/get-{_downloadInfo[downloadInfoUrl]}/{sign}/{ts}/{path}";
        }

        /// <summary>
        /// Returns similar tracks
        /// </summary>
        /// <param name="trackId">Track ID of the track you need to get similar tracks</param>
        /// <returns>A list with similar tracks to the one you passed on</returns>
        public async Task<JObject> GetTrackSimilar(int trackId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>() {
                { "accept", "application/json" }
            };
            
            var result = await PostGet.GetAsync($"tracks/{trackId}/similar", header);
            return result;
        }

        /// <summary>
        /// Getting tracks the user doesn't like
        /// </summary>
        /// <param name="userId">User ID (insert the ID to which the token belongs)</param>
        /// <returns>Returns a library with a list of dislikes tracks, their id and album id</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> GetDislikesTracks(string userId)
        {
            
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            var result = await PostGet.GetAsync($"users/{userId}/dislikes/tracks", header);
            return result;
        }

        /// <summary>
        /// Receiving an addendum to a track (lyrics, clip, video)
        /// </summary>
        /// <param name="trackId">Track ID</param>
        /// <returns>Returns a list of additional information about the song, lyrics, clip availability, videos</returns>
        public async Task<JObject> GetSupplement(int trackId)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };
            
            var result = await PostGet.GetAsync($"tracks/{trackId}/supplement", header);
            return result;
        }
        /// <summary>
        /// Retrieving song lyrics (with timecodes)
        /// </summary>
        /// <param name="trackId">ID Track</param>
        /// <returns>downloadUrl with song lyrics and timecodes, the author of the lyrics, and the service where the lyrics come from</returns>
        public async Task<JObject> GetLyric(int trackId)
        {
            CheckToken();
            SignModel singKey = GetSingKey(new GetLyricRequest { TrackId = trackId });
            Dictionary<string, string> headers = new Dictionary<string, string> { { "X-Yandex-Music-Client", "YandexMusicAndroid/24023621" } };
            
            var result = await PostGet.GetAsync($"tracks/{trackId}/lyrics?format=LRC&timeStamp={singKey.Timestamp}&sign={singKey.Value}", headers, noDefaultHeaders: true);
            return result;
        }
        
        /// <summary>
        /// Get the Sign key (signature) to get the track and get the lyrics.
        /// </summary>
        /// <param name="request">
        /// An instance of <see cref="GetLyricRequest"/> or <see cref="FetchTrackLinkRequest"/> is passed, depending on the situation
        /// </param>
        /// <returns>
        /// <see cref="SignModel"/> containing the Sign key and a Timestamp with the time the key was created.
        /// </returns>
        /// <exception cref="ArgumentException">If an object is passed that is not <see cref="GetLyricRequest"/> or <see cref="FetchTrackLinkRequest"/></exception>
        private SignModel GetSingKey(object request)
        {
            int timestamp = (int)DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            string key = String.Empty;
            string message;

            if (request is GetLyricRequest lyricRequest)
            {
                message = $"{lyricRequest.TrackId}{timestamp}";
                key = DefaultSignKey;
            }
            else if (request is FetchTrackLinkRequest fetchTrackLinkRequest)
            {
                message =
                    $"{timestamp}{fetchTrackLinkRequest.TrackId}{fetchTrackLinkRequest.Quality}{fetchTrackLinkRequest.Codec.Replace(",","")}{fetchTrackLinkRequest.Transport}";
                key = FetchTrackLinkSignKey;
            }
            else
                throw new ArgumentException("Invalid request type");

            using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(key)))
            {
                byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
                string sign = Convert.ToBase64String(hashBytes);

                if (request is FetchTrackLinkRequest)
                    sign = sign.TrimEnd('=');
                
                sign = HttpUtility.UrlEncode(sign);
                return new SignModel { Timestamp = timestamp, Value = sign };
            }

        }

    }
}
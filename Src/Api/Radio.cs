using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels.Radio;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is needed to retrieve radio station tracks, station information, retrieve radio stations from the home page and retrieve a list of radio stations
    /// </summary>
    public class Radio : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and NetworkParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, language and UserAgent installed</param>
        public Radio(ApiParams apiParams) : base(apiParams) { }
        
        /// <summary>
        /// Obtaining a list of stations
        /// </summary>
        /// <param name="page">Page list of radio stations (optional, default 0)</param>.
        /// <param name="pageSize">Page size of radio list output (optional, default is 10)</param>.
        /// <returns>List with all available radio stations</returns>
        public async Task<JObject> StationList(int page = 0, int pageSize = 10)
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "application/json" }
            };

            return await PostGet.GetAsync($"rotor/stations/list?page={page}&page-size={pageSize}", header);
        }

        /// <summary>
        /// Reverts my wave's previous settings
        /// </summary>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <returns>My Wave Previous Settings</returns>
        public async Task<JObject> WaveLast()
        {
            CheckToken();
            return await PostGet.GetAsync("rotor/wave/last");
        }

        /// <summary>
        /// Returns all possible settings for the wave you specified
        /// </summary>
        /// <param name="idWave">Wave ID, for example: user:onyourwave</param>
        /// <returns>List with Possible Wave Settings</returns>
        public async Task<JObject> GetWaveSettings(string idWave)
        {
            return await PostGet.GetAsync($"rotor/wave/settings?seeds={idWave}");
        }

        /// <summary>
        /// Creating a Session for a Wave
        /// </summary>
        /// <param name="settingsRotor">The wave settings you need are passed here <see cref="GetWaveSettings"/>, be sure to add the wave ID</param>
        /// <returns>Wave session ID and the first 5 tracks from there</returns>
        public async Task<JObject> NewSession(List<string> settingsRotor)
        {
            JArray modes = new JArray { settingsRotor };
            JObject mainJson = new JObject
            {
                { "includeTracksInResponse", true },
                { "includeWaveModel", true },
                { "seeds", modes },
            };

            return await PostGet.PostAsync("rotor/session/new", new StringContent(mainJson.ToString()));
        }

        /// <summary>
        /// Sending feedback about the tracks that the wave gives you (track started, like, skip, etc.). Perform when all 5 tracks are finished
        /// </summary>
        /// <param name="radioSessionId">Radio station ID, can be obtained from the NewSession, WaveLast method</param>
        /// <param name="feedbackList">List with feedback about tracks. First of all, add 2 types, RadioStarted and TrackStarted, then according to the situation (if the user skips, add skip, and so on). Fill in this list until you run out of 5 tracks</param>
        /// <param name="from">You can get it from the GetInformAboutRadio method, why Yandex needs this information is not quite clear., the default is "radio-mobile-user-onyourwave-default"</param>
        /// <returns>List with 5 tracks from the wave</returns>
        public async Task<JObject> Feedback(string radioSessionId, List<FeedbackData> feedbackList, string from = "radio-mobile-user-onyourwave-default")
        {
            CheckToken();
            JArray feedbacksArray = new JArray();

            foreach (var feedbackData in feedbackList)
            {
                string timeStamp = feedbackData.TimeStamp ?? DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+00:00");
                JObject feedbackEvent = new JObject(
                    new JProperty("timestamp", timeStamp),
                    new JProperty("type", GetEnumDescription(feedbackData.FeedbackType))
                );

                JObject feedback = new JObject(new JProperty("event", feedbackEvent), new JProperty("from", from));

                switch (feedbackData)
                {
                    case TrackStartedFeedbackData ts:
                        feedbackEvent.Add(new JProperty("trackId", ts.TrackId));
                        feedback.Add(new JProperty("batchId", ts.BatchId));
                        break;
                    case TrackFinishedFeedbackData tf:
                        feedbackEvent.Add(new JProperty("trackId", tf.TrackId));
                        feedbackEvent.Add(new JProperty("totalPlayedSeconds", tf.TotalPlayedSecond));
                        feedbackEvent.Add(new JProperty("trackLengthSeconds", tf.TrackLengthSeconds));
                        feedback.Add(new JProperty("batchId", tf.BatchId));
                        break;
                    case SkipFeedbackData sk:
                        feedbackEvent.Add(new JProperty("trackId", sk.TrackId));
                        feedbackEvent.Add(new JProperty("totalPlayedSeconds", sk.TotalPlayedSecond));
                        feedback.Add(new JProperty("batchId", sk.BatchId));
                        break;
                    case LikeFeedbackData lk:
                        feedbackEvent.Add(new JProperty("trackId", lk.TrackId));
                        feedback.Add(new JProperty("batchId", lk.BatchId));
                        break;
                    case UnlikeFeedbackData ul:
                        feedbackEvent.Add(new JProperty("trackId", ul.TrackId));
                        feedback.Add(new JProperty("batchId", ul.BatchId));
                        break;
                }

                feedbacksArray.Add(feedback);
            }

            JObject feedbackJson = new JObject(new JProperty("feedbacks", feedbacksArray));
            return await PostGet.PostAsync($"rotor/session/{radioSessionId}/tracks", new StringContent(feedbackJson.ToString()));
        }

        /// <summary>
        /// Getting information about the wave
        /// </summary>
        /// <param name="radioSessionId">Wave ID, can be obtained from NewSession or from WaveLast</param>
        /// <returns>What the settings of this wave, when it was created, the name and id of the station</returns>
        public async Task<JObject> GetInformAboutRadio(string radioSessionId)
        {
            CheckToken();

            return await PostGet.GetAsync($"rotor/session/{radioSessionId}?includeWaveModel=true");
        }
    }
}
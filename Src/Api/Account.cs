using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using YandexMusicApi.Network;

namespace YandexMusicApi.Api
{
    /// <summary>
    /// This class is needed to retrieve information about the user's account, retrieve and modify its settings and activate promo codes
    /// </summary>
    public class Account : BaseApi
    {
        /// <summary>
        /// This constructor takes a token to use it in methods and ApiParams to use a proxy and UserAgent
        /// </summary>
        /// <param name="apiParams">ApiParams class object with proxy, UserAgent and language installed</param>
        public Account(ApiParams apiParams) : base(apiParams) { }

        /// <summary>
        /// Shows whether the account participates in Yandex experiments. It requires a token to work
        /// </summary>
        /// <returns>All experiments in which you participate (or do not participate) will be displayed there. If you try to make a request without a token, the result for an unauthorized user will be returned</returns>
        public async Task<JObject> Expirements()
        {
            Dictionary<string, string> header = new Dictionary<string, string>()
            {
                { "accept", "*/*" }
            };

            var result = await PostGet.GetAsync($"account/experiments", header);
            return result;
        }

        /// <summary>
        /// Activates a promo code on your account
        /// </summary>
        /// <returns>The result of activating a promo code, successful or not</returns>
        /// <param name="promocode">Promo code that you need to activate</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> Promocode(string promocode)
        {
            CheckToken();
            Dictionary<string, string> data = new Dictionary<string, string>() { { "code", promocode } };
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };
            
            var result = await PostGet.PostAsync($"account/consume-promo-code", new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Outputs the account settings as well as the uuid account
        /// </summary>
        /// <returns>Shows all account settings</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>.
        public async Task<JObject> ShowSettings()
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };
            
            var result = await PostGet.GetAsync("account/settings", header);
            return result;
        }

        /// <summary>
        /// Changes the account settings
        /// </summary>
        /// <returns>All your settings (allows you to check if the settings have changed or not)</returns>
        /// <param name="data">Settings that need to be changed</param>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        /// <example>
        /// An example of what the data should look like to change the settings
        /// <code>
        /// Dictionary&lt;string, string&gt; data = new Dictionary&lt;string, string&gt;()
        /// {
        ///     { "childModEnabled", "false" }
        /// };
        /// </code>
        /// </example>
        public async Task<JObject> SettingsChanged(Dictionary<string, string> data)
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };
            
            var result = await PostGet.PostAsync("account/settings", new FormUrlEncodedContent(data), header);
            return result;
        }

        /// <summary>
        /// Outputs information about the account, uuid, first name, last name, login, phone number and so on
        /// </summary>
        /// <returns>Account information, region, basic information (first name, last name, etc.), rights, subscription information, plus</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> ShowInformAccount()
        {
            CheckToken();
            Dictionary<string, string> header = new Dictionary<string, string>() { { "accept", "application/json" } };
            
            var result = await PostGet.GetAsync("account/status", header);
            return result;
        }

        /// <summary>
        /// Displays brief account information (used in the beta version of Yandex Music upon program launch).
        /// </summary>
        /// <returns>User UID, avatar ID (https://avatars.mds.yandex.net/get-yapic/{Insert obtained ID}/islands-middle), name, username, and country with language.</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> ShowBriefAccountInform()
        {
            CheckToken();
            var result = await PostGet.GetAsync("account/about");
            return result;
        }
        
        /// <summary>
        /// Returns account information from Yandex Passport (Yandex ID)
        /// </summary>
        /// <returns>Email, first name, last name, avatar_id to get the user's avatar (https://yandex.ru/dev/id/doc/ru/user-information#avatar-access), id, login, psuid</returns>
        /// <exception cref="ArgumentException">This method requires a token from Yandex music, it is specified in the class constructor</exception>
        public async Task<JObject> ShowInformAccountFromYandexPassport()
        {
            CheckToken();
            var result = await PostGet.GetAsync("https://login.yandex.ru/info?format=json");
            return result;
        }
    }
}
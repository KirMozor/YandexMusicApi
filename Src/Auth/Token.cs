using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YandexMusicApi.ComponentModels;
using YandexMusicApi.Network;

namespace YandexMusicApi.Auth
{
    /// <summary>
    /// Class to log in to your account and get a token from YandexMusic
    /// </summary>
    public class Token
    {
        private readonly Dictionary<string, string> _authPayload = new Dictionary<string, string>();
        
        private string _retpathUrl = "https://oauth.yandex.ru/authorize?response_type=token&client_id=23cabbbdc6cd418abb4b39c32c41195d";
        private int _phoneId;
        private string _challengeType = String.Empty;
        private string _captchaKey = String.Empty;
        private string _requestId = String.Empty;
        private Dictionary<string, string> _defaultHeaders = new Dictionary<string, string>() { { "X-Requested-With", "XMLHttpRequest" } };
        
        /*private readonly string _username = String.Empty;
        private readonly string _password = String.Empty;*/
        private readonly PostGet _postGet;
        private readonly ApiParams _apiParams;

        /// <summary>
        /// Initializes a new instance of the <see cref="Token"/> class.
        /// </summary>
        /// <param name="apiParams">The API parameters.</param>
        public Token(ApiParams apiParams)
        {
            _apiParams = apiParams;
            _postGet = new PostGet(apiParams);
        }
        
        /// <summary>
        /// Initiates the authentication process to the Yandex Music API by validating the username or phone number.
        /// </summary>
        /// <param name="username">The username for authentication.</param>
        /// <remarks>
        /// This method starts the authentication process by validating the username or phone number. It does not complete the full authentication process.
        /// Based on the response, the user can decide how to proceed with the authentication (e.g., password, two-factor authentication, etc.).
        /// This method performs several steps to authenticate:
        /// 1. Retrieves the process_uuid from the Yandex OAuth page.
        /// 2. Retrieves the csrf_token from the Yandex Passport page.
        /// 3. Submits a POST request to the Yandex Passport start page with the necessary authentication data.
        /// 4. Checks the response to determine if the user can register or authorize.
        /// 5. If the user can authorize, the track_id is retrieved from the response and stored for future requests.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object that contains the server's response to the login request.
        /// </returns>
        /// <exception cref="System.Exception">Thrown when the user cannot authorize.</exception>
        public async Task<LoginResponse> LoginUsername(string username)
        {
            // Getting process_uuid for further use in queries
            var responseProcessUuid = await _postGet.GetAsync(
                "https://oauth.yandex.ru/authorize?response_type=token&client_id=23cabbbdc6cd418abb4b39c32c41195d", ifConvertToJson: false);
            var rawProcessUuid = Regex.Match(responseProcessUuid["result"].ToString(), "\"process_uuid\":\"([^\"]+)\"");
            var processUuid = rawProcessUuid.Groups[1].Value;

            // Getting csrf_token for all requests
            var responseCsrfToken = await _postGet.GetAsync("https://passport.yandex.ru/am?app_platform=android", ifConvertToJson: false);
            var rawCsrfToken = Regex.Match(responseCsrfToken["result"].ToString(), "\"csrf_token\" value=\"([^\"]+)\"");
            _authPayload["csrf_token"] = rawCsrfToken.Groups[1].Value;

            // Getting track_id for all requests and also to check if there is an account and the possibility of further authorization
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "login", username },
                { "process_uuid", processUuid },
                { "retpath", _retpathUrl },
                { "origin" , "oauth" },
                { "check_for_xtokens_for_pictures", "1" }
            };
            var authLoginResponse = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/auth/multi_step/start", content: new FormUrlEncodedContent(data), _defaultHeaders);

            var jsonAuthLoginResponse = JObject.Parse(authLoginResponse["result"].ToString());
            if (jsonAuthLoginResponse.ContainsKey("can_register") && jsonAuthLoginResponse["can_register"]?.Value<bool>() == true)
                return new LoginResponse(JObject.FromObject(new { errors = new List<string> { "account.not_found" } }));
            if (jsonAuthLoginResponse["can_authorize"]?.Value<bool>() != true)
                throw new Exception(authLoginResponse["result"].ToString());

            _authPayload["track_id"] = jsonAuthLoginResponse["track_id"]?.Value<string>();

            return new LoginResponse(jsonAuthLoginResponse);
        }
        
        /// <summary>
        /// Authenticates the user by validating the password.
        /// </summary>
        /// <param name="password">The password for authentication.</param>
        /// <remarks>
        /// This method completes the authentication process by validating the password. It handles different responses from the server:
        /// 
        /// Success:
        /// - If no challenge is required, the server responds with a status of "ok" and a "retpath" URL.
        /// - If a challenge (e.g., SMS, push notification, etc.) is required, the server responds with a status of "ok", a state of "auth_challenge", an "avatarId", and a "redirect_url".
        /// 
        /// Error:
        /// - If the password does not match, the server responds with a status of "error" and an error message of "password.not_matched".
        /// - If a captcha is required, the server responds with a status of "error" and an error message of "captcha.required".
        /// 
        /// This method performs a POST request to the Yandex Passport commit password page with the necessary authentication data.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object that contains the server's response to the login request.
        /// </returns>
        public async Task<LoginResponse> LoginPassword(string password)
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "password", password },
                { "retpath", _retpathUrl },
                { "lang", _apiParams.AcceptLanguage }
            };
            var responseLoginPassword = await _postGet.PostAsync(
                "https://passport.yandex.ru/registration-validations/auth/multi_step/commit_password",
                content: new FormUrlEncodedContent(data),
                _defaultHeaders);
            return new LoginResponse(JObject.Parse(responseLoginPassword["result"].ToString()));
        }
        
        /// <summary>
        /// Checks the type of challenge required by Yandex for authentication.
        /// </summary>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object that contains the server's response to the challenge check request.
        /// </returns>
        public async Task<LoginResponse> CheckChallenge()
        {
            var checkChallengeResponse = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/auth/challenge/submit",
                content: new FormUrlEncodedContent(_authPayload));
            var jsonResponse = JObject.Parse(checkChallengeResponse["result"].ToString());
            
            _challengeType = jsonResponse["challenge"]["challengeType"].ToString();
            if (_challengeType == "mobile_id" || _challengeType == "phone_confirmation")
                _phoneId = jsonResponse["challenge"]["challengeInfo"][_challengeType]["phone_id"].Value<int>();

            return new LoginResponse(jsonResponse);
        }

        /// <summary>
        /// Sends a request to Yandex to get a Mobile ID code.
        /// </summary>
        /// <remarks>
        /// This method sends a POST request to Yandex to get a Mobile ID code for authentication. 
        /// The server responds with a status of "ok" if the request is successful.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object that contains the server's response to the Mobile ID code request.
        /// </returns>
        public async Task<LoginResponse> MobileIdGetCode()
        {
            var mobileIdPush = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/auth/challenge/send_push",
                content: new FormUrlEncodedContent(_authPayload));
            return new LoginResponse(JObject.Parse(mobileIdPush["result"].ToString()));
        }
        
        /// <summary>
        /// Confirms the Mobile ID challenge by sending the code received from the user.
        /// </summary>
        /// <remarks>
        /// This method sends a POST request to Yandex to confirm the Mobile ID challenge. The server responds with a status of "error" and an error message of "challenge.not_passed" if the code is incorrect.
        /// </remarks>
        /// <param name="smsCode">The code received from the user.</param>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object that contains the server's response to the Mobile ID confirmation request.
        /// </returns>
        public async Task<LoginResponse> MobileIdConfirm(int smsCode)
        {
            _challengeType = "push_2fa";
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "challenge", _challengeType },
                { "answer", smsCode.ToString() }
            };
            
            var mobileIdPush = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/auth/challenge/commit", 
                content: new FormUrlEncodedContent(data));
            return new LoginResponse(JObject.Parse(mobileIdPush["result"].ToString()));
        }
        
        /// <summary>
        /// Requests Yandex to send an SMS code to a phone number for verification.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a POST request to Yandex to send an SMS to a phone number for verification.
        /// The request includes the phone identifier (`phone_id`) and confirmation method (`by_sms_only`).
        /// 
        /// The server responds with a JSON object parsed into a `LoginResponse` object.
        /// The response can indicate success ("ok") or error (details vary based on the error).
        /// 
        /// This method is typically used before the user receives an SMS code from Yandex.
        /// </remarks>
        /// <returns>
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the server's response.
        /// </returns>
        public async Task<LoginResponse> PhoneConfirmCodeSubmit()
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "phone_id", _phoneId.ToString() },
                //{ "confirm_method", "by_sms_only" },
                { "isCodeWithFormat", "true" },
                { "scenario", "CHALLENGE_AUTH" }
            };
            _challengeType = "phone_confirmation";

            var phoneConfirmCodeSubmit = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/phone-confirm-code-submit",
                content: new FormUrlEncodedContent(data),
                _defaultHeaders);
            return new LoginResponse(JObject.Parse(phoneConfirmCodeSubmit["result"].ToString()));
        }

        /// <summary>
        /// Sends an SMS code received from Yandex to confirm phone number verification.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a POST request to Yandex to confirm the phone number with the SMS code provided by the user to verify the login and subsequently receive the token.
        /// The request includes the confirmation code (`code`).
        /// 
        /// The server responds with a JSON object parsed into a `LoginResponse` object. The response can indicate success ("ok") or error (details vary based on the error).
        /// 
        /// This method is typically used after the user has received an SMS code from Yandex and entered it into the application.
        /// </remarks>
        /// <param name="smsCode">The SMS confirmation code received from Yandex.</param>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the server's response.
        /// </returns>
        public async Task<LoginResponse> PhoneConfirmCodeSend(string smsCode)
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "code", smsCode }
            };
            var phoneConfirmCode = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/phone-confirm-code", content: new FormUrlEncodedContent(data),
                _defaultHeaders);
            return new LoginResponse(JObject.Parse(phoneConfirmCode["result"].ToString()));
        }
        
        /// <summary>
        /// Retrieves a CAPTCHA challenge from Yandex.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a POST request to Yandex to retrieve a CAPTCHA challenge.
        /// The request includes the language (`language`) and scale factor (`scale_factor`).
        /// 
        /// The server responds with a JSON object parsed into a `LoginResponse` object. The response contains the CAPTCHA challenge key (`key`) and other relevant information.
        /// 
        /// This method is typically called when Yandex indicates that a CAPTCHA challenge is required for authentication.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the CAPTCHA challenge and other response data.
        /// </returns>
        public async Task<LoginResponse> GetCaptcha()
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "language", _apiParams.AcceptLanguage },
                { "scale_factor", "3" }
            };
            var getCaptchaResponse = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/textcaptcha", content: new FormUrlEncodedContent(data));
            var jsonCaptchaResponse = JObject.Parse(getCaptchaResponse["result"].ToString());
            _captchaKey = jsonCaptchaResponse["key"]?.Value<string>();
            
            return new LoginResponse(jsonCaptchaResponse);
        }
        
        /// <summary>
        /// Submits a CAPTCHA response to Yandex.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a POST request to Yandex to submit a CAPTCHA response.
        /// The request includes the CAPTCHA response (`answer`) and the CAPTCHA key (`key`).
        /// 
        /// The server responds with a JSON object parsed into a `LoginResponse` object. The response indicates whether the CAPTCHA was solved correctly ("ok") or not (error details vary based on the error).
        /// 
        /// This method is typically called after the user has solved the CAPTCHA and the response needs to be submitted to Yandex.
        /// </remarks>
        /// <param name="answer">The CAPTCHA response provided by the user.</param>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the server's response to the CAPTCHA submission.
        /// </returns>
        public async Task<LoginResponse> SendCaptcha(string answer)
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "answer", answer },
                { "key", _captchaKey }
            };
            var sendCaptchaResponse = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/checkHuman", content: new FormUrlEncodedContent(data));
            return new LoginResponse(JObject.Parse(sendCaptchaResponse["result"].ToString()));
        }

        /// <summary>
        /// Sending an code to an email
        /// </summary>
        /// <returns>JSON with the result, successful or not</returns>
        public async Task<LoginResponse> SendEmailCode()
        {
            var startEmailCodeResponse =
                await _postGet.PostAsync(
                    "https://passport.yandex.ru/registration-validations/auth/challenge/send_code_to_email", content: new FormUrlEncodedContent(_authPayload));
            return new LoginResponse(startEmailCodeResponse);
        }

        /// <summary>
        /// Sending a code from email
        /// </summary>
        /// <param name="emailCode">The code that came to your email</param>
        /// <returns>JSON with the result of the send, the code is correct or not</returns>
        public async Task<LoginResponse> CheckEmailCode(int emailCode)
        {
            var data = new Dictionary<string, string>()
            {
                { "csrf_token", _authPayload["csrf_token"] },
                { "code", emailCode.ToString() },
                { "trackId", _authPayload["track_id"]}
            };
            
            var sendEmailCode =
                await _postGet.PostAsync(
                    "https://passport.yandex.ru/registration-validations/auth/challenge/confirm_email_code", content: new FormUrlEncodedContent(data));

            return new LoginResponse(sendEmailCode);
        }
        
        /// <summary>
        /// Completes an additional verification challenge requested by Yandex.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a POST request to report that additional validation from Yandex is complete (such as SMS code or code from the application).
        /// The request includes the challenge type (`challenge`).
        /// 
        /// The server responds with a JSON object parsed into a `LoginResponse` object. The response indicates whether the challenge was completed successfully ("ok") or not (error details vary based on the error).
        /// 
        /// This method is typically called after the user has completed the additional verification step, such as entering a code from a push notification or SMS message.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the server's response to the challenge completion.
        /// </returns>
        public async Task<LoginResponse> CommitChallenge()
        {
            var data = new Dictionary<string, string>(_authPayload)
            {
                { "challenge", _challengeType }
            };
            var commitChallenge = await _postGet.PostAsync("https://passport.yandex.ru/registration-validations/auth/challenge/commit", content: new FormUrlEncodedContent(data));
            return new LoginResponse(JObject.Parse(commitChallenge["result"].ToString()));
        }

        /// <summary>
        /// Retrieves an access token from Yandex Music.
        /// </summary>
        /// <remarks>
        /// This asynchronous method sends a GET request to Yandex Music to obtain an access token.
        /// The request is sent to the authorization endpoint with the client ID and response type set to "token".
        /// 
        /// The server responds with a URL containing the access token. The method extracts the access token from the response using a regular expression and returns it in a `LoginResponse` object.
        /// 
        /// This method is typically called after the user has successfully completed all authentication steps, such as entering their login credentials and verifying any additional challenges.
        /// The access token can then be used to make authorized requests to Yandex Music APIs.
        /// </remarks>
        /// <returns>
        /// A <see cref="Task{TResult}"/> representing the asynchronous operation.
        /// The <see cref="Task{TResult}.Result"/> property returns a <see cref="LoginResponse"/> object containing the access token or an error message if the token could not be retrieved.
        /// </returns>
        public async Task<LoginResponse> GetToken()
        {
            var rawTokenResponse =
                await _postGet.GetAsync(
                    "https://oauth.yandex.ru/authorize?response_type=token&client_id=23cabbbdc6cd418abb4b39c32c41195d", ifConvertToJson: false);
            var accessTokenMatch = Regex.Match(rawTokenResponse["result"].ToString(), @"access_token=([^&]+)");
            if (accessTokenMatch.Success)
            {
                string accessToken = accessTokenMatch.Groups[1].Value;
                return new LoginResponse(new JObject(
                    new JProperty("status", "ok"),
                    new JProperty("message", accessToken)));
            }
            else
            {
                try
                {
                    IConfiguration config = Configuration.Default;

                    IBrowsingContext context = BrowsingContext.New(config);
                    IDocument document = await context.OpenAsync(req => req.Content(rawTokenResponse["result"].ToString()));

                    var mainElement = document.GetElementById("root");
                    var dataReduxState = mainElement.GetAttribute("data-redux-state");
                    var json = JsonConvert.DeserializeObject<JObject>(dataReduxState);
                    _requestId = json["authorize"]["form"][12]["value"].ToString();

                    if (!string.IsNullOrEmpty(_requestId))
                    {
                        return new LoginResponse(
                            new JObject(
                                new JProperty("status", "error_access_account"),
                                new JProperty("message", "Allow YandexMusic to access your account")));
                    }
                    else
                    {
                        return new LoginResponse(
                            new JObject(
                                new JProperty("status", "error"),
                                new JProperty("message", "token not found")));
                    }
                }
                catch (Exception ex)
                {
                    return new LoginResponse(
                        new JObject(
                            new JProperty("status", "error"),
                            new JProperty("message", $"There was an error while parsing _requestId, please report to Issue on GitLab: {ex}")));
                }
            }
        }
    }
}
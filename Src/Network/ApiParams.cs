using System.Net.Http.Headers;

namespace YandexMusicApi.Network
{
    /// <summary>
    /// The class that represents the parameters of the network connection for use by the PostGet class and other classes that use HTTP requests.
    /// </summary>
    public class ApiParams
    {
        /// <summary>
        /// The address of the proxy server.
        /// </summary>
        public string Proxy { get; set; } = null;
        /// <summary>
        /// The username to authenticate to the proxy server.
        /// </summary>
        public string ProxyUsername { get; set; } = null;
        /// <summary>
        /// The password to authenticate the proxy server.
        /// </summary>
        public string ProxyPassword { get; set; } = null;

        /// <summary>
        /// Token from Yandex Music, without it many functions will be unavailable and full versions of the song will be impossible to get
        /// </summary>
        public string TokenYandexMusic { get; set; } = null;
        /// <summary>
        /// The header User-Agent sent as part of the request.
        /// </summary>
        public string UserAgent { get; set; } =
            "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0";

        /// <summary>
        /// The language in which the answers to the requests are written
        /// </summary>
        public string AcceptLanguage { get; set; } = "en";
    }
}
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace YandexMusicApi.Network
{
    /// <summary>
    /// Main class for sending PostGet requests to Yandex Music Api
    /// </summary>
    internal class PostGet
    {
        private static bool _messageDisplayed;
        private readonly HttpClient _httpClient;
        private readonly CookieContainer _сookieContainer = new CookieContainer();

        /// <summary>
        /// Constructor for the PostGet class.
        /// </summary>
        internal PostGet(ApiParams apiParams)
        {
            if (!_messageDisplayed)
            {
                string message = @"
            YandexMusicApi - Open-source api written in C# for YandexMusic service
            Author: KirMozor (link: https://gitlab.com/KirMozor)
            License: Apache 2.0
            Repository link: https://gitlab.com/Xeronix/YandexMusicApi
        ";
                if (IsWpfApplication())
                    Trace.WriteLine(message);
                else
                    Console.WriteLine(message);
                _messageDisplayed = true;
            }
            
            var httpClientHandler = new HttpClientHandler
            {
                UseCookies = true,
                CookieContainer = _сookieContainer
            };
            
            httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (!string.IsNullOrEmpty(apiParams.Proxy))
            {
                httpClientHandler.Proxy = new WebProxy(apiParams.Proxy);
                httpClientHandler.UseProxy = true;

                if (!string.IsNullOrEmpty(apiParams.ProxyUsername) && !string.IsNullOrEmpty(apiParams.ProxyPassword))
                {
                    httpClientHandler.Proxy.Credentials = new NetworkCredential(apiParams.ProxyUsername, apiParams.ProxyPassword);
                    httpClientHandler.UseDefaultCredentials = false;
                }
            }

            _httpClient = new HttpClient(httpClientHandler)
            {
                BaseAddress = new Uri("https://api.music.yandex.net:443"),
                DefaultRequestHeaders =
                {
                    AcceptLanguage = { StringWithQualityHeaderValue.Parse(apiParams.AcceptLanguage) }
                }
            };
            _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(apiParams.UserAgent);
            _httpClient.DefaultRequestHeaders.Add("range", "bytes=0-10485759");
            _httpClient.DefaultRequestHeaders.Add("X-Yandex-Music-Client", "YandexMusicDesktopAppWindows/5.13.2");
            _httpClient.DefaultRequestHeaders.Add("X-Yandex-Music-Device", 
                "os=Windows.Desktop; os_version=10.0.22621.1265; manufacturer=Gigabyte Technology Co., Ltd.; model=Z370P D3; clid=WindowsPhone; device_id=03003236030060AC050008740500A19E050044A8060001000400AA1F010058040200CA3A0900E8A6; uuid=generated-by-music-e5cee301-f4d5-466b-8afd-97a68699cf0c");
           _httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            
            if (apiParams.TokenYandexMusic != null)
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", apiParams.TokenYandexMusic);
        }

        /// <summary>
        /// Sends an HTTP GET request to the specified URL and returns the response as a string.
        /// </summary>
        /// <param name="url">The URL to send the GET request to.</param>
        /// <param name="headers">A dictionary of headers to be added to the request (optional).</param>
        /// <param name="noDefaultHeaders">Disables standard headers for the request (false by default)</param>
        /// <param name="ifConvertToJson">Whether to convert the response from the server to JSON. Default, yes</param>
        /// <returns>A string containing the response body.</returns>
        /// <exception cref="ArgumentException">Thrown if the provided URL is blank or null.</exception>
        public async Task<JObject> GetAsync(string url, Dictionary<string, string> headers = null,
            bool noDefaultHeaders = false, bool ifConvertToJson = true)
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentException("URL is blank, insert link", nameof(url));

            var request = new HttpRequestMessage(HttpMethod.Get, url);
            
            // Add default headers
            if (!noDefaultHeaders)
                foreach (var defaultHeader in _httpClient.DefaultRequestHeaders)
                    request.Headers.TryAddWithoutValidation(defaultHeader.Key, defaultHeader.Value);
            
            // Add custom headers
            if (headers != null)
                foreach (var header in headers)
                    request.Headers.TryAddWithoutValidation(header.Key, header.Value);

            HttpResponseMessage response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead);
            if (ifConvertToJson)
            {
                string resultResponse = await response.Content.ReadAsStringAsync();
                JObject resultConvert = JsonConvert.DeserializeObject<JObject>(resultResponse);

                return new JObject
                {
                    { "status", response.StatusCode.ToString() },
                    { "result", resultConvert }
                };
            }
            else
                return new JObject
                {
                    { "status", response.StatusCode.ToString() },
                    { "result",  await response.Content.ReadAsStringAsync()}
                };
        }

        /// <summary>
        /// Sends an HTTP POST request to the specified URL with the specified content and headers and returns the response body as a string asynchronously.
        /// </summary>
        /// <param name="url">The URL to which the request is sent.</param>
        /// <param name="content">The HTTP content to send.</param>
        /// <param name="headers">The headers to include in the request (optional).</param>
        /// <param name="noDefaultHeaders">Disables standard headers for the request (false by default)</param>
        /// <param name="ifConvertToJson">Whether to convert the response from the server to JSON. Default, yes</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains the response body as a string.</returns>
        /// <exception cref="ArgumentException">Thrown when the URL is null or empty.</exception>
        public async Task<JObject> PostAsync(string url, HttpContent content = null,
            Dictionary<string, string> headers = null, bool noDefaultHeaders = false,
            bool ifConvertToJson = true)
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentException("URL is blank, insert link", nameof(url));

            var request = new HttpRequestMessage(HttpMethod.Post, url);

            // Add default headers
            if (!noDefaultHeaders)
                foreach (var defaultHeader in _httpClient.DefaultRequestHeaders)
                    request.Headers.TryAddWithoutValidation(defaultHeader.Key, defaultHeader.Value);
            
            // Add custom headers
            if (headers != null)
                foreach (var header in headers)
                    request.Headers.TryAddWithoutValidation(header.Key, header.Value);

            if (content != null)
                request.Content = content;

            HttpResponseMessage response = await _httpClient.SendAsync(request);
            if (ifConvertToJson)
            {
                string resultResponse = await response.Content.ReadAsStringAsync();
                JObject resultConvert = JsonConvert.DeserializeObject<JObject>(resultResponse);

                return new JObject
                {
                    { "status", response.StatusCode.ToString() },
                    { "result", resultConvert }
                };
            }
            else
                return new JObject
                {
                    { "status", response.StatusCode.ToString() },
                    { "result", response.Content.ReadAsStringAsync().ToString() }
                };
        }
        /// <summary>
        /// This method determines whether the library is used in WPF or not (bypassing the when the WPF program freezes dead because of the Console.WriteLine();)
        /// </summary>
        /// <returns>Returns true if WPF and vice versa</returns>
        private static bool IsWpfApplication()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                if (assembly.FullName.StartsWith("PresentationFramework", StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
